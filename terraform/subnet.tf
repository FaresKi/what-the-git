resource "aws_subnet" "wtg_public_subnet" {
  vpc_id                  = aws_vpc.wtg_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zone
  tags = {
    Name = "wtg-${var.env}-public-subnet"
  }
}

resource "aws_subnet" "wtg_private_subnet" {
  vpc_id                  = aws_vpc.wtg_vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zone
  tags = {
    Name = "wtg-${var.env}-private-subnet"
  }
}