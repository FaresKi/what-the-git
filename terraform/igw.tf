resource "aws_internet_gateway" "wtg_igw" {
  vpc_id = aws_vpc.wtg_vpc.id

  tags = {
    Name = "wtg-${var.env}-igw"
  }
}