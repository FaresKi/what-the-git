## ROUTE TABLES
resource "aws_route_table" "wtg_public_rt" {
  vpc_id = aws_vpc.wtg_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.wtg_igw.id
  }

  tags = {
    Name = "wtg-${var.env}-public-rt"
  }
}

## ASSOCIATIONS
resource "aws_route_table_association" "wtg_rt_association_public" {
  subnet_id      = aws_subnet.wtg_public_subnet.id
  route_table_id = aws_route_table.wtg_public_rt.id
}