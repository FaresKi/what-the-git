## SG
resource "aws_security_group" "wtg_frontend_sg" {
  vpc_id = aws_vpc.wtg_vpc.id
  name   = "wtg-${var.env}-frontend-sg"
  ingress {
    description      = "INGRESS_HTTPS_ALL"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "INGRESS_HTTP_ALL"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "SSH_FARES"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["90.79.67.174/32"]
  }

  egress {
    description      = "EGRESS_ALL"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
