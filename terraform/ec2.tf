data "aws_ami" "wtg_ami" {
  most_recent = true
  filter {
    name   = "tag:Name"
    values = ["wtg-${var.env}-ami"]
  }
  owners = ["self"]
}

data "aws_iam_instance_profile" "S3RoleForWTG-DEV" {
  name = "S3RoleForWTG-DEV"
}

resource "aws_instance" "wtg-ec2" {
  ami                    = data.aws_ami.wtg_ami.id
  instance_type          = "t2.medium"
  key_name               = "neovendo-key"
  vpc_security_group_ids = [aws_security_group.wtg_frontend_sg.id]
  subnet_id              = aws_subnet.wtg_public_subnet.id
  tags = {
    Name = "wtg-${var.env}-app"
  }
  iam_instance_profile = data.aws_iam_instance_profile.S3RoleForWTG-DEV.name
  user_data            = <<-EOF
        #!/bin/bash
        cd /home/ubuntu/what-the-git/
        git checkout GIT_BRANCH
        sudo rm -rf /etc/nginx/sites-available/default
        sudo mv nginx.default /etc/nginx/sites-available/default
        sudo certbot --nginx -d whatthegit.ninja -d www.whatthegit.ninja --non-interactive --agree-tos -m kissoumfares@gmail.com
        cd backend/
        aws s3 cp s3://wtg-dev/backend/.env ./
        if [ -f .env ]; then
          export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
        fi
        sudo docker run -d --name postgres-db -v "$PWD/db/scripts/init.sql":/docker-entrypoint-initdb.d/init.sql -p $POSTGRES_PORT:5432 -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_DB=$POSTGRES_DB -d postgres:13.1-alpine
        pm2 start app.js
        cd ../frontend/
        aws s3 cp s3://wtg-dev/frontend/.env ./
        sudo npm run build
        sudo systemctl status nginx
        sudo service nginx restart  
  EOF
}

output "wtg_instance_ip_address" {
  value = aws_instance.wtg-ec2.public_ip
}
