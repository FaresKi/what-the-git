# What the Git?
`What the Git?` is a tool that provides insights on various Git related statistics. It is designed for project managers and allow them to have a clear vision on their team members' performances through a refined dashboard experience. Using various data, `What the Git?` is able to build concise reports on a member's activity down to the Git branch level. It is reachable [here](https://whatthegit.ninja).

## Concept
`What the Git?` uses Git-providers' API (GitLab/GitHub) to grab all the data it needs. Using your Git Identity (basically your GitHub/GitLab account), it hoovers all repositories you are participating in and builds a list of all the contributors to those. Then, you are able to start a new team by adding members (from the list sniffed above). On a given period, you are able to search for team's Git statistics on a per-member view.

## MVP & MMFs
### Accounts, Security & Permissions
Users should be able to login to the app. It implies that an account is either created by the user or provided by Git client's authentication APIs (SSO for instance).

A user can create teams. They only access teams he created. Once created a team can be either modified (adding/removing members, changing its profile picture or its name) or deleted by its owner.

A user can add or remove linking to its Git-providers' accounts through the user profile management page.

### Statistics 
> NOTE: At the time this README.md was written, we don't know yet which Git-providers' API we will use. Could be both and will depend mainly on the complexity of those APIs.

The data used in the app are provided by either GitLab or GitHub APIs. It implies that you must link either your GitHub.com or GitLab.com account (depends on which API is supported at that time).

As such, it only provides data you already have access to and gather it in one place to perform various calculations and produce a report.

Examples :
* Commits
* Changes
* Merge requests
* Issues
* Code statistics
* ...

### Features
Once they select a team, a user can search for a time period and access team's statistics related to it. Those statistics are gathered in a `report` which consists on `Members view` and a `Details view`.
* `Members view` : Quick summary on each team members performances
* `Details view` : More detailed insights about the selected member

## Enhancements (according to project MMFs completion)
### Implemenation of the remaining Git-provider API (if any)
If either GitHub's API or GitLab's API is not supported, add support to it and integrates it in the app.

### Provide access (R/W) to team report to other users
Implementing a more elaborated access policy on users (maybe through roles?).

### Adding a project view
On a given Git repository, provides insights related to it.

## Moodboard (preview ?)
> NOTE: This preview only gives the general idea of what we want. The actual app might differ in some apsects.
> <img src="https://media.discordapp.net/attachments/773533707851661325/774015796640219146/MainPage2x.png?width=1194&height=671"/>

## How to make it run?
> Note that for both the back and the front end, you will need specifics environment variables. For obvious reasons, they're not hosted in this repository. We can provide it to you :D

> Additionnaly, Backend must run on port 3000, Frontend must run on port 3001 and Postgres must be accessible from 5159 port :)
### Postgres
```
docker run -p 5159:5432 --name postgres-db -e POSTGRES_PASSWORD=<POSTGRES_PASSWORD> -e POSTGRES_DB=wtg -e POSTGRES_USER=wtg_admin -d postgres:13.1-alpine
```

### Backend
This will install all dependencies:
```
npm install
```
This will launch the tests and instanciate the database on Docker. 
```
npm test
```
```
npm install
```
```
npm start
```
### Frontend
To install all dependencies:
```
npm install
```
For production environments:
```
npm run build
```
## Contributors
###  Mohammad Baraa AL BAKRI <mohammad.baraa.al.bakri@efrei.net> 
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@Baraa-ALBAKRI](https://github.com/Baraa-ALBAKRI) 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@Baraa-](https://gitlab.com/Baraa-) 

###  Nicolas BENCHIMOL <nicolas.benchimol@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@Bechamelle](https://github.com/Bechamelle) 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@Bechamelle](https://gitlab.com/Bechamelle) 

###  Emrick DONADEI <emrick.donadei@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@edonadei](https://github.com/edonadei) 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@edonadei](https://gitlab.com/edonadei) 

###  Laurent HAZEBROUCQ <laurent.hazebroucq@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@Haseirbrook](https://github.com/Haseirbrook) 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@Haseirbrook](https://gitlab.com/Haseirbrook) 

###  Fares KISSOUM <fares.kissoum@efrei.net>
<img src="https://cdn4.iconfinder.com/data/icons/miu-flat-social/60/github-512.png" width=25> [@FaresKi](https://github.com/FaresKi) 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width=25> [@FaresKi](https://gitlab.com/FaresKi) 
