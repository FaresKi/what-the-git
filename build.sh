#! /bin/sh
echo '----------------------Installing docker...----------------------'
sudo apt-get update
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get install unzip
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo gpasswd -a $USER docker
echo '----------------------Installing Nginx...----------------------'
sudo apt update
sudo apt install -y nginx
sudo service nginx start
echo '----------------------Installing Certbot...----------------------'
sudo apt-get install -y python3-certbot-nginx
echo '----------------------Installing AWS-CLI...----------------------'
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
echo '----------------------Installing NodeJS...----------------------'
wget -qO- https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install -y nodejs
echo '----------------------Installing pm2 dependency to launch front and back as processes----------------------'
sudo npm install -g pm2
sudo npm install -g serve
echo '----------------------Cloning the repo...----------------------'
git clone https://oauth:$TOKEN@gitlab.com/FaresKi/what-the-git.git
cd what-the-git
git checkout $BRANCH
echo '----------------------Installing node dependencies...----------------------'
cd frontend/
npm install
cd ../backend/
npm install