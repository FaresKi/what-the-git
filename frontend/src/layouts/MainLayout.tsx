import React from "react";

export const MainLayout = (props: any) => {
  return (
    <div className="flex-grow w-full max-w-7xl mx-auto xl:px-8 lg:flex">
      {/*  Left sidebar & main wrapper  */}
      <div className="flex-1 min-w-0 bg-white xl:flex">{props.children}</div>
    </div>
  );
};
