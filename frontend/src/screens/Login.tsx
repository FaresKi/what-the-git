import GitlabLogo from "assets/gitlab-logo-gray-stacked-rgb.svg";
import Axios from "axios";
import OauthPopup from "components/OauthPopup";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { CTX } from "context";

export const Login = () => {
  let history = useHistory();
  const { setUser } = useContext(CTX);

  return (
    <div className="min-h-screen bg-white flex">
      <div className="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
        <div className="mx-auto w-full max-w-sm">
          <div>
            <h2 className="mt-6 text-3xl leading-9 font-extrabold text-gray-900">
              Sign in to your account
            </h2>
          </div>

          <div className="mt-8">
            <div>
              <div>
                <div className="mt-1 grid grid-cols-3 gap-3">
                  <div>
                    <span className="w-full inline-flex rounded-md shadow-sm">
                      <OauthPopup
                        title="Gitlab connection"
                        height={600}
                        width={600}
                        onClose={() => {
                          Axios.get(process.env.REACT_APP_BACKEND_URL + "/auth/me", {
                            withCredentials: true,
                          })
                            .then((res) => {
                              if (res.status === 200) {
                                setUser(res.data);
                                localStorage.setItem("WTGUser", JSON.stringify(res.data));
                                history.push("/team");
                              } else {
                                console.log("Retry to connect :(");
                              }
                            })
                            .catch((err) => console.log(err));
                        }}
                        url={process.env.REACT_APP_BACKEND_URL + "/auth/gitlab"}
                      >
                        <button className="w-full inline-flex justify-center py-2 px-4 border border-gray-300 rounded-md bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition duration-150 ease-in-out">
                          <img width="80" src={GitlabLogo} alt="Gitlab logo" />
                        </button>
                      </OauthPopup>
                    </span>
                  </div>
                </div>
              </div>

              <div className="mt-6 relative">
                <div className="absolute inset-0 flex items-center">
                  <div className="w-full border-t border-gray-300" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:block relative w-0 flex-1">
        <img
          className="absolute inset-0 h-full w-full object-cover"
          src="https://images.unsplash.com/photo-1556761175-4b46a572b786?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80"
          alt=""
        />
      </div>
    </div>
  );
};
