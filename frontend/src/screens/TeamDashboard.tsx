import Axios from "axios";
import { CalendarPicker } from "components/CalendarPicker";
import { CTX } from "context";
import { Activity, Contributor, RangeDate, Squad, User } from "interfaces";
import { MainLayout } from "layouts/MainLayout";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { AccountProfile } from "../components/AccountProfile";
import { ActivityFeed } from "../components/ActivityFeed";
import { MemberList } from "../components/MemberList";

interface TeamDashboardProps {
  user: string;
}

export const TeamDashboard: React.FC<TeamDashboardProps> = () => {
  const { user } = useContext(CTX);
  const [rangeDate, setRangeDate] = useState<RangeDate>({
    from: new Date(),
    to: new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000),
  });
  const [currentUser, setCurrentUser] = useState<User>(user);
  const [contributors, setContributors] = useState<Contributor[]>([]);
  const [selectedSquad, setSelectedSquad] = useState<Squad | undefined>();
  const [selectedSquadDetails, setSelectedSquadDetails] = useState<Squad | undefined>();
  const [loadingDetails, setLoadingDetails] = useState<boolean>(false);
  const [loadingActivity, setLoadingActivity] = useState<boolean>(false);
  const [memberSelected, setMemberSelected] = useState<User | undefined>();
  const [activityMemberSelected, setActivityMemberSelected] = useState<Activity | undefined>();

  useEffect(() => {
    setCurrentUser(user);
  }, [user]);
  // Récupere les détails de la squad en fonction de la date
  useEffect(() => {
    try {
      setLoadingDetails(true);
      if (selectedSquad) {
        Axios.get(
          `${process.env.REACT_APP_BACKEND_URL}/api/v1/squads/${selectedSquad?.squad_id}/details`,
          {
            headers: {
              startdate: moment(rangeDate.from).format("YYYY-MM-DD"),
              enddate: moment(rangeDate.to).format("YYYY-MM-DD"),
            },
            withCredentials: true,
          }
        )
          .then((res) => {
            let squadDetails: Squad = res.data;
            squadDetails.members = squadDetails.members.filter(
              (member) => member.activity !== null
            );
            setSelectedSquadDetails(squadDetails);
            setLoadingDetails(false);
          })
          .catch((err) => console.log(err));
      }
    } catch (err) {
      console.log(err);
      setLoadingDetails(false);
    }
  }, [selectedSquad, rangeDate]);

  useEffect(() => {
    Axios.get(process.env.REACT_APP_BACKEND_URL + "/api/v1/gitlab/contributors", {
      withCredentials: true,
    })
      .then((data) => {
        setContributors(data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <MainLayout>
      <AccountProfile
        currentUser={currentUser}
        setCurrentUser={setCurrentUser}
        contributors={contributors}
        setSelectedSquad={setSelectedSquad}
      />
      <CalendarPicker rangeDate={rangeDate} setRangeDate={setRangeDate} />
      <MemberList
        selectedSquadDetails={selectedSquadDetails}
        loadingDetails={loadingDetails}
        setMemberSelected={setMemberSelected}
        memberSelected={memberSelected}
        rangeDate={rangeDate}
        setActivityMemberSelected={setActivityMemberSelected}
        currentUser={currentUser}
        setLoadingActivity={setLoadingActivity}
      />
      <ActivityFeed
        memberSelected={memberSelected}
        activityMemberSelected={activityMemberSelected}
        loadingActivity={loadingActivity}
      />
    </MainLayout>
  );
};
