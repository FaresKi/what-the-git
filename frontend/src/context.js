import Axios from "axios";
import React, { useState } from "react";
import { sortSquadsOfUser } from "utils/utils";

export const CTX = React.createContext();

export const Context = (props) => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("WTGUser")));

  const handleAddingNewTeam = async (teamName) => {
    Axios.post(process.env.REACT_APP_BACKEND_URL + "/api/v1/squads", null, {
      headers: { displayName: teamName },
      withCredentials: true,
    })
      .then(() => handleGetCurrentUser())
      .catch((err) => console.log(err));
  };

  const handleGetCurrentUser = () => {
    Axios.get(process.env.REACT_APP_BACKEND_URL + "/auth/me", {
      withCredentials: true,
    })
      .then((res) => {
        if (res.status === 200) {
          if (res.data.squads) {
            let userToReturn = sortSquadsOfUser(res.data);
            localStorage.setItem("WTGUser", JSON.stringify(userToReturn));
            setUser(userToReturn);
          } else {
            localStorage.removeItem("WTGUser");
            setUser(null);
          }
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <CTX.Provider value={{ user, handleAddingNewTeam, setUser }}>{props.children}</CTX.Provider>
  );
};
