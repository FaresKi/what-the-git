export interface User {
  user_id: number;
  gitlab: GitlabUser;
  squads: Squad[];
  activity: Activity;
}

export interface GitlabUser {
  profile_id: number;
  name: string;
  username: string;
  avatar_url: string;
  web_url: string;
}

export interface Squad {
  squad_id: number;
  picture_url: string;
  name: string;
  owner: SquadOwner;
  members: User[];
}

export interface SquadOwner {
  user_id: number;
  gitlab: GitlabUser;
  squad_id: number;
}

export interface Contributor {
  user_id: number;
  gitlab: GitlabUser;
  activity: Activity;
}

export interface Activity {
  languages: Language;
  last_commit: Commit;
  commits: Commit[];
}

export interface Commit {
  commit_id: string;
  project_id: number;
  branch: string;
  title: string;
  committed_date: string;
  project_name: string;
  project_url: string;
}

export interface Language {
  [key: string]: number;
}

export interface Member {
  firstName: string;
  lastName: string;
  gitName: string;
  gitUrl: string;
  languages: Language[];
}

export interface Team {
  name: string;
  members: Member[];
}

export interface RangeDate {
  from: Date;
  to: Date;
}
