import { Activity, User } from "interfaces";
import moment from "moment";
import React from "react";
import ReactLoading from "react-loading";

interface ActivityFeedProps {
  memberSelected: User | undefined;
  activityMemberSelected: Activity | undefined;
  loadingActivity: boolean;
}

export const ActivityFeed: React.FC<ActivityFeedProps> = ({
  memberSelected,
  activityMemberSelected,
  loadingActivity,
}) => {
  const now = moment();

  return (
    <div className="bg-gray-50 pr-4 sm:pr-6 lg:pr-8 lg:flex-shrink-0 lg:border-l lg:border-gray-200 xl:pr-0">
      <div className="pl-6 lg:w-80">
        <div className="pt-6 pb-2">
          <h2 className="text-sm leading-5 font-semibold">Activity</h2>
        </div>
        {loadingActivity ? (
          <ReactLoading type={"spin"} color={"gray"} height={"20%"} width={"20%"} />
        ) : null}
        {memberSelected && !loadingActivity ? (
          <div>
            <ul className="divide-y divide-gray-200">
              {activityMemberSelected?.commits.map((commit, key) => (
                <li key={key} className="py-4">
                  <div className="flex space-x-3">
                    <img
                      className="h-6 w-6 rounded-full"
                      src={memberSelected?.gitlab?.avatar_url}
                      alt=""
                    />
                    <div className="flex-1 space-y-1">
                      <div className="flex items-center justify-between">
                        <h3 className="text-sm font-medium leading-5">
                          {memberSelected?.gitlab?.name}
                        </h3>
                        <p className="text-sm leading-5 text-gray-500">
                          {now.diff(moment(commit.committed_date), "hours")}h
                        </p>
                      </div>
                      <a
                        rel="noopener noreferrer"
                        target={"_blank"}
                        href={memberSelected.activity.last_commit.project_url}
                      >
                        <p className="text-sm leading-5 text-gray-500">
                          {commit.title} on {commit.project_name}
                        </p>
                      </a>
                    </div>
                  </div>
                </li>
              ))}

              {/*  More items...  */}
            </ul>
            <div className="py-4 text-sm leading-5 border-t border-gray-200"></div>
          </div>
        ) : (
          <p className="text-base m-4">No member selected. Click on one to see activity</p>
        )}
      </div>
    </div>
  );
};
