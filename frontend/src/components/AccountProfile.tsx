import Axios from "axios";
import { CTX } from "context";
import { Contributor, Squad, User } from "interfaces";
import React, { useContext, useState } from "react";
import ReactLoading from "react-loading";
import Select from "react-select";
import { sortSquadsOfUser } from "utils/utils";

interface AccountProfileProps {
  currentUser: User;
  contributors: Contributor[];
  setCurrentUser: React.Dispatch<React.SetStateAction<User>>;
  setSelectedSquad: React.Dispatch<React.SetStateAction<Squad | undefined>>;
}

export const AccountProfile: React.FC<AccountProfileProps> = ({
  currentUser,
  setCurrentUser,
  contributors,
  setSelectedSquad,
}) => {
  const [addingNewTeam, setaddingNewTeam] = useState(false);
  const [newTeamName, setNewTeamName] = useState("");
  const [selectedSquadIndex, setSelectedSquadIndex] = useState(0);
  const { handleAddingNewTeam } = useContext(CTX);

  React.useEffect(() => {
    if (currentUser) {
      setSelectedSquad(currentUser.squads[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentUser]);

  return (
    <div className="xl:flex-shrink-0 xl:w-64 xl:border-r xl:border-gray-200 bg-white">
      <div className="pl-4 pr-6 py-6 sm:pl-6 lg:pl-8 xl:pl-0">
        <div className="flex items-center justify-between">
          <div className="flex-1 space-y-8">
            <div className="space-y-8 sm:space-y-0 sm:flex sm:justify-between sm:items-center xl:block xl:space-y-8">
              {currentUser ? (
                <React.Fragment>
                  {/*  Profile  */}
                  <div className="flex items-center space-x-3">
                    <div className="flex-shrink-0 h-12 w-12">
                      <img
                        className="h-12 w-12 rounded-full"
                        src={currentUser?.gitlab?.avatar_url}
                        alt=""
                      />
                    </div>
                    <div className="space-y-1">
                      <div className="text-sm leading-5 font-medium text-gray-900">
                        {currentUser?.gitlab?.name}
                      </div>
                      <a href="/#" className="group flex items-center space-x-2.5">
                        <svg
                          className="w-5 h-5 text-gray-400 group-hover:text-gray-500"
                          viewBox="0 0 18 18"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8.99917 0C4.02996 0 0 4.02545 0 8.99143C0 12.9639 2.57853 16.3336 6.15489 17.5225C6.60518 17.6053 6.76927 17.3277 6.76927 17.0892C6.76927 16.8762 6.76153 16.3104 6.75711 15.5603C4.25372 16.1034 3.72553 14.3548 3.72553 14.3548C3.31612 13.316 2.72605 13.0395 2.72605 13.0395C1.9089 12.482 2.78793 12.4931 2.78793 12.4931C3.69127 12.5565 4.16643 13.4198 4.16643 13.4198C4.96921 14.7936 6.27312 14.3968 6.78584 14.1666C6.86761 13.5859 7.10022 13.1896 7.35713 12.965C5.35873 12.7381 3.25756 11.9665 3.25756 8.52116C3.25756 7.53978 3.6084 6.73667 4.18411 6.10854C4.09129 5.88114 3.78244 4.96654 4.27251 3.72904C4.27251 3.72904 5.02778 3.48728 6.74717 4.65082C7.46487 4.45101 8.23506 4.35165 9.00028 4.34779C9.76494 4.35165 10.5346 4.45101 11.2534 4.65082C12.9717 3.48728 13.7258 3.72904 13.7258 3.72904C14.217 4.96654 13.9082 5.88114 13.8159 6.10854C14.3927 6.73667 14.7408 7.53978 14.7408 8.52116C14.7408 11.9753 12.6363 12.7354 10.6318 12.9578C10.9545 13.2355 11.2423 13.7841 11.2423 14.6231C11.2423 15.8247 11.2313 16.7945 11.2313 17.0892C11.2313 17.3299 11.3937 17.6097 11.8501 17.522C15.4237 16.3303 18 12.9628 18 8.99143C18 4.02545 13.97 0 8.99917 0Z"
                            fill="currentcolor"
                          />
                        </svg>
                        <div className="text-sm leading-5 ml-1 text-gray-500 group-hover:text-gray-900 font-medium">
                          {currentUser?.gitlab?.username}
                        </div>
                      </a>
                    </div>
                  </div>

                  {/* Adding a team */}
                  <span className="inline-flex rounded-md shadow-sm mt-5">
                    <button
                      onClick={(_) => {
                        setaddingNewTeam(true);
                      }}
                      type="button"
                      className="w-full inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
                    >
                      New Team
                    </button>
                  </span>
                  {addingNewTeam ? (
                    <div className="mb-3 pt-0">
                      <input
                        onKeyDown={(event) => {
                          if (event.key === "Enter") {
                            setaddingNewTeam(false);
                            handleAddingNewTeam(newTeamName);
                          }
                        }}
                        onChange={(event) => {
                          setNewTeamName(event.target.value);
                        }}
                        value={newTeamName}
                        type="text"
                        placeholder="Type the team name"
                        className="px-3 py-3 placeholder-gray-500 text-gray-700 relative bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-full"
                      />
                    </div>
                  ) : null}

                  {/*  Team list  */}
                  <div className="flex flex-col space-y-3 sm:space-y-0 sm:space-x-3 sm:flex-row xl:flex-col xl:space-x-0 xl:space-y-3">
                    {currentUser?.squads
                      ? currentUser?.squads.map((squad, key) => (
                          <React.Fragment>
                            <button
                              key={key}
                              className={`flex flex-row items-center hover:bg-gray-100 ${
                                key === selectedSquadIndex ? "bg-gray-300" : null
                              } rounded-xl p-2`}
                              onClick={() => {
                                setSelectedSquadIndex(key);
                                setSelectedSquad(squad);
                              }}
                            >
                              <div className="flex items-center justify-center h-8 w-8 bg-indigo-200 rounded-full">
                                {squad.name.charAt(0).toUpperCase()}
                              </div>
                              <div className="ml-2 text-sm font-semibold">
                                {squad.name.charAt(0).toUpperCase()}
                                {squad.name.slice(1)}
                              </div>
                              <div>
                                <svg
                                  onClick={() =>
                                    Axios.delete(
                                      process.env.REACT_APP_BACKEND_URL +
                                        "/api/v1/squads/" +
                                        squad.squad_id,
                                      {
                                        withCredentials: true,
                                      }
                                    )
                                      .then(() => {
                                        let newCurrentUser = { ...currentUser };
                                        newCurrentUser.squads = newCurrentUser.squads.filter(
                                          (squadToEdit) => squadToEdit.squad_id !== squad.squad_id
                                        );
                                        setCurrentUser(sortSquadsOfUser(newCurrentUser));
                                        localStorage.setItem(
                                          "WTGUser",
                                          JSON.stringify(sortSquadsOfUser(newCurrentUser))
                                        );
                                      })
                                      .catch((err) => console.log(err))
                                  }
                                  className="-mr-1 ml-2 h-5 w-5"
                                  xmlns="http://www.w3.org/2000/svg"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                >
                                  <path d="M6 18L18 6M6 6l12 12" />
                                </svg>
                              </div>
                            </button>
                            <Select
                              isMulti
                              placeholder="Manage the members..."
                              options={contributors.map((contributor) => {
                                return {
                                  value: contributor.user_id,
                                  label: contributor.gitlab.name,
                                };
                              })}
                              value={squad.members.map((member) => {
                                return {
                                  value: member.user_id,
                                  label: member.gitlab.name,
                                };
                              })}
                              onChange={(_, action) => {
                                let headers = {};
                                if (action.action === "select-option") {
                                  headers = {
                                    displayname: squad.name,
                                    membersids: `[${[
                                      ...squad.members.map((member) => member.user_id),
                                      action.option?.value,
                                    ].toString()}]`,
                                    ownerid: squad.owner.user_id.toString(),
                                  };
                                } else if (action.action === "remove-value") {
                                  headers = {
                                    displayname: squad.name,
                                    membersids: `[${[
                                      ...squad.members.filter(
                                        (member) => member.user_id !== action.removedValue?.value
                                      ),
                                    ]
                                      .map((member) => member.user_id)
                                      .toString()}]`,
                                    ownerid: squad.owner.user_id.toString(),
                                  };
                                }

                                const apiUrl =
                                  process.env.REACT_APP_BACKEND_URL +
                                  "/api/v1/squads/" +
                                  squad.squad_id;
                                Axios.put(apiUrl, _, {
                                  headers: headers,
                                  withCredentials: true,
                                })
                                  .then((res) => {
                                    const newTeam = res.data;
                                    let newCurrentUser = { ...currentUser };
                                    newCurrentUser.squads = newCurrentUser.squads.filter(
                                      (squadToEdit) => squadToEdit.squad_id !== squad.squad_id
                                    );
                                    newCurrentUser.squads.push(newTeam);
                                    setCurrentUser(sortSquadsOfUser(newCurrentUser));
                                    localStorage.setItem(
                                      "WTGUser",
                                      JSON.stringify(sortSquadsOfUser(newCurrentUser))
                                    );
                                  })
                                  .catch((err) => console.log(err));
                              }}
                            ></Select>
                          </React.Fragment>
                        ))
                      : null}
                  </div>
                  {/*  Meta info  */}
                  <div className="flex flex-col space-y-6 sm:flex-row sm:space-y-0 sm:space-x-8 xl:flex-col xl:space-x-0 xl:space-y-6">
                    <div className="flex items-center space-x-2">
                      {/*  Heroicon name: collection  */}
                      <svg
                        className="h-5 w-5 text-gray-400"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path d="M7 3a1 1 0 000 2h6a1 1 0 100-2H7zM4 7a1 1 0 011-1h10a1 1 0 110 2H5a1 1 0 01-1-1zM2 11a2 2 0 012-2h12a2 2 0 012 2v4a2 2 0 01-2 2H4a2 2 0 01-2-2v-4z" />
                      </svg>
                      <span className="text-sm text-gray-500 leading-5 font-medium">
                        {`${currentUser.squads.length} teams`}
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <ReactLoading type={"spin"} color={"gray"} height={"20%"} width={"20%"} />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
