import moment from "moment";
import React from "react";
import Helmet from 'react-helmet';
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import {RangeDate} from "interfaces";

interface CalendarPickerProps {
  rangeDate: RangeDate;
  setRangeDate: React.Dispatch<React.SetStateAction<RangeDate>>;
}

export const CalendarPicker: React.FC<CalendarPickerProps> = ({
  rangeDate,
  setRangeDate,
}) => {

  const modifiers = { start: rangeDate.from, end: rangeDate.to };

  const handleDayChange = (day: Date) => {
    setRangeDate(DateUtils.addDayToRange(day,rangeDate))
  };

  function convertToDuration(
    unit: string
  ): moment.unitOfTime.DurationConstructor {
    if (unit === "day" || unit === "week" || unit === "month") {
      return unit;
    } // Default unit is hours
    else return "hours";
  }
  const setRangeDayClick = (option: string) => {
    switch (option) {
      case "yesterday":
        setdayRange(option);
        break;
      case "day":
        setdayRange(option);
        break;
      case "week":
        setdayRange(option);
        break;
      case "month":
        setdayRange(option);
        break;
      default:
        break;
    }
  };
  const setdayRange = (option: string) => {
    //Possible input : "week","month","year" ...
    if(option === "yesterday")
    {          
      let ytdaystartDate = new Date(new Date().setDate(moment().startOf(convertToDuration("day")).toDate().getDate() - 1));
      let ytdayendDate = new Date(new Date().setDate(moment().endOf(convertToDuration("day")).toDate().getDate() - 1));

      setRangeDate({from : ytdaystartDate ,to : ytdayendDate})
    }
    else
    {
      let startingDate = moment().startOf(convertToDuration(option)).toDate();
      let endDate = moment().endOf(convertToDuration(option)).toDate();
      setRangeDate({from : startingDate,to : endDate})
    }
  };
  return (
    <div className="bg-white lg:min-w-0 lg:flex-1 border-r border-gray-200">
      <div className="pl-4 pr-6 pt-4 pb-4 border-b border-t border-gray-200 sm:pl-6 lg:pl-8 xl:pl-6 xl:pt-6 xl:border-t-0">
        <div className="flex items-center">
          <h1 className="flex-1 text-lg leading-7 font-medium">Calendar</h1>
        </div>
      </div>
      <ul className="relative z-0 divide-y divide-gray-200">
        <li className="relative pl-4 pr-6 py-5 hover:bg-gray-50 sm:py-6 sm:pl-6 lg:pl-8 xl:pl-6">
          
          <button
            onClick={() => setRangeDayClick("yesterday")}
            className="ml-3 w-100 inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
          >
            Yesterday
          </button>
          <button
            onClick={() => setRangeDayClick("day")}
            className="ml-3 w-100 inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
          >
            Today
          </button>
          <button
            onClick={() => setRangeDayClick("week")}
            className="ml-3 w-100 inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
          >
            This week
          </button>
          <button
            onClick={() => setRangeDayClick("month")}
            className="ml-3 w-100 inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150"
          >
            This month
           </button>
          <div className="flex items-center justify-between space-x-4">
            {/*  Repo name and link  */}
            <DayPicker
              className="Selectable"
              numberOfMonths={2}
              //selectedDays={dates}
              selectedDays={[rangeDate.from, rangeDate]}
              modifiers={modifiers}             
              onDayClick={(day) => 
              {
                handleDayChange(day);
              }}
            />
          </div>
        </li>

      </ul>
      <Helmet>
          <style>{`
  .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
    background-color: #f0f8ff !important;
    color: #4a90e2;
  }
  .Selectable .DayPicker-Day {
    border-radius: 0 !important;
  }
  .Selectable .DayPicker-Day--start {
    border-top-left-radius: 50% !important;
    border-bottom-left-radius: 50% !important;
  }
  .Selectable .DayPicker-Day--end {
    border-top-right-radius: 50% !important;
    border-bottom-right-radius: 50% !important;
  }
`}</style>
        </Helmet>
    </div>
  );
};
