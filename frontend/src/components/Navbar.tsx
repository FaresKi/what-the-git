import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { CTX } from "context";
import Axios from "axios";

export const Navbar = () => {
  const { user, setUser } = React.useContext(CTX);
  const [mobileMenuVisible, setMobileMenuVisible] = useState(false);
  let history = useHistory();

  return (
    <nav className="flex-shrink-0 bg-indigo-700">
      <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          {/*  Logo section */}
          <div className="flex items-center px-2 lg:px-0 xl:w-64">
            <div className="flex-shrink-0">
              <Link to="/">
                <img
                  className="h-8 w-auto"
                  src="https://tailwindui.com/img/logos/v1/workflow-mark-on-brand.svg"
                  alt="Workflow logo"
                />
              </Link>
            </div>
          </div>

          <div className="flex lg:hidden">
            {/*  Mobile menu button */}
            <button
              className="inline-flex items-center justify-center p-2 rounded-md text-indigo-400 hover:text-white hover:bg-indigo-600 focus:outline-none focus:bg-indigo-600 focus:text-white transition duration-150 ease-in-out"
              aria-label="Main menu"
              aria-expanded="false"
              onClick={() => {
                setMobileMenuVisible(!mobileMenuVisible);
              }}
            >
              {/*  Icon when menu is closed. */}
              {/* 
              Heroicon name: menu-alt-1

              Menu open: "hidden", Menu closed: "block"
            */}
              <svg
                className="block h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M4 6h16M4 12h8m-8 6h16" />
              </svg>
              {/*  Icon when menu is open. */}
              {/* 
              Heroicon name: x

              Menu open: "block", Menu closed: "hidden"
            */}
              <svg
                className="hidden h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          {/*  Links section */}
          <div className="hidden lg:block lg:w-80">
            <div className="flex items-center justify-end">
              <div className="flex">
                {!user ? (
                  <Link
                    to="/login"
                    className="px-3 py-2 rounded-md text-sm leading-5 font-medium text-indigo-200 hover:text-white focus:outline-none focus:text-white focus:bg-indigo-600 transition duration-150 ease-in-out"
                  >
                    Sign in
                  </Link>
                ) : (
                  <React.Fragment>
                    <Link
                      to="/team"
                      className="px-3 py-2 rounded-md text-sm leading-5 font-medium text-indigo-200 hover:text-white focus:outline-none focus:text-white focus:bg-indigo-600 transition duration-150 ease-in-out"
                    >
                      Your team
                    </Link>
                    <Link
                      to="/"
                      onClick={(e) => {
                        e.preventDefault();
                        Axios.get(process.env.REACT_APP_BACKEND_URL + "/auth/logout", {
                          withCredentials: true,
                        })
                          .then((res) => {
                            setUser(null);
                            localStorage.removeItem("WTGUser");
                            history.push("/");
                          })
                          .catch((err) => console.log(err));
                      }}
                      className="px-3 py-2 rounded-md text-sm leading-5 font-medium text-indigo-200 hover:text-white focus:outline-none focus:text-white focus:bg-indigo-600 transition duration-150 ease-in-out"
                    >
                      Log out
                    </Link>
                  </React.Fragment>
                )}
              </div>
              {/*  Profile dropdown */}
              <div className=" hidden ml-4 relative flex-shrink-0">
                <div>
                  <button
                    className="flex text-sm rounded-full text-white focus:outline-none focus:shadow-solid transition duration-150 ease-in-out"
                    id="user-menu"
                    aria-label="User menu"
                    aria-haspopup="true"
                  >
                    <img
                      className="h-8 w-8 rounded-full"
                      src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
                      alt=""
                    />
                  </button>
                </div>
                {/* 
                Profile dropdown panel, show/hide based on dropdown state.

                Entering: "transition ease-out duration-100"
                  From: "transform opacity-0 scale-95"
                  To: "transform opacity-100 scale-100"
                Leaving: "transition ease-in duration-75"
                  From: "transform opacity-100 scale-100"
                  To: "transform opacity-0 scale-95"
              */}
                <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
                  <div
                    className="py-1 rounded-md bg-white shadow-xs"
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="user-menu"
                  >
                    <a
                      href="/#"
                      className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                      role="menuitem"
                    >
                      View Profile
                    </a>
                    <a
                      href="/#"
                      className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                      role="menuitem"
                    >
                      Settings
                    </a>
                    <a
                      href="/#"
                      className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                      role="menuitem"
                    >
                      Logout
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* 
      Mobile menu, toggle classes based on menu state.
      Menu open: "block", Menu closed: "hidden"
    */}
      <div className={` ${mobileMenuVisible ? "" : "hidden"} lg:hidden`}>
        <div className="px-2 pt-2 pb-3">
          {!user ? (
            <Link
              to="/login"
              className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-indigo-200 hover:text-indigo-100 hover:bg-indigo-600 focus:outline-none focus:text-white focus:bg-indigo-600 transition duration-150 ease-in-out"
            >
              Sign in
            </Link>
          ) : (
            <React.Fragment>
              <Link
                to="/team"
                className="block px-3 py-2 rounded-md text-base font-medium text-white hover:bg-indigo-600  focus:outline-none focus:text-indigo-100 focus:bg-indigo-600 transition duration-150 ease-in-out"
              >
                Your team
              </Link>
              <Link
                      to="/"
                      onClick={(e) => {
                        e.preventDefault();
                        Axios.get(process.env.REACT_APP_BACKEND_URL + "/auth/logout", {
                          withCredentials: true,
                        })
                          .then((res) => {
                            setUser(null);
                            localStorage.removeItem("WTGUser");
                            history.push("/");
                          })
                          .catch((err) => console.log(err));
                      }}
                      className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-indigo-200 hover:text-indigo-100 hover:bg-indigo-600 focus:outline-none focus:text-white focus:bg-indigo-600 transition duration-150 ease-in-out"
                    >
                      Log out
                    </Link>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};
