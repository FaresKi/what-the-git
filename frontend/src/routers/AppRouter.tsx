import React from "react";
import { Route, Switch } from "react-router-dom";
import { LandingPage } from "../screens/LandingPage";
import { Login } from "../screens/Login";
import { TeamDashboard } from "../screens/TeamDashboard";

export const AppRouter: React.FC = () => {
  return (
    <Switch>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/register">
        <Login />
      </Route>
      <Route exact path="/">
        <LandingPage />
      </Route>
      <Route path="/team">
        <TeamDashboard user="me" />
      </Route>
    </Switch>
  );
};
