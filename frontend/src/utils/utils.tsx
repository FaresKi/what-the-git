import { Squad, User } from "interfaces";

function compareSquads(a: Squad, b: Squad) {
  if (a.name < b.name) {
    return -1;
  }
  if (a.name > b.name) {
    return 1;
  }
  return 0;
}

export const sortSquadsOfUser = (user: User) => {
  user.squads.sort(compareSquads);
  return user;
};
