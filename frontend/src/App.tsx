import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { AppRouter } from "routers/AppRouter";
import { Navbar } from "components/Navbar";
import { Context } from "context";

function App() {
  return (
    <Context>
      <Router>
        <Navbar />
        <AppRouter />
      </Router>
    </Context>
  );
}

export default App;
