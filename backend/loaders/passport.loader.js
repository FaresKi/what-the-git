const passport = require('passport');
const GitLabStrategy = require('passport-gitlab2').Strategy;
const GitProfiles = require('../db/models/git-profiles');
const MockStrategy = require('../test/mocked-class/mock-strategy');
const Crypto = require('../utils/crypto');
module.exports = function(app) {
  app.use(passport.initialize());
  app.use(passport.session());

  // eslint-disable-next-line require-jsdoc
  async function identifyUser(accessToken, refreshToken, profile, cb) {
    let gitlabProfile = await GitProfiles.findGitProfile(
        profile.id,
        'gitlab',
    );
    const encryptedAccessToken = Crypto.encrypt(accessToken);
    const encryptedRefreshToken = Crypto.encrypt(refreshToken);
    if (gitlabProfile == null) {
      gitlabProfile = await GitProfiles.createGitProfile(
          profile.id,
          'gitlab',
          encryptedAccessToken,
          encryptedRefreshToken,
          null,
      );
    } else {
      gitlabProfile = await GitProfiles.updateGitProfile(
          profile.id,
          'gitlab',
          encryptedAccessToken,
          encryptedRefreshToken,
          gitlabProfile.USER_ID,
      );
    }

    const gitUser = {
      id: gitlabProfile.USER_ID,
      gitlab: {
        id: profile.id,
        access_token: accessToken,
        refresh_token: refreshToken,
      },
    };
    return cb(null, gitUser);
  }

  /**
   * @return {MockStrategy}
   */
  function buildMockStrategy() {
    return new MockStrategy('gitlab', identifyUser);
  }

  /**
   * @return {GitLabStrategy}
   */
  function buildGitlabStrategy() {
    return new GitLabStrategy(
        {
          clientID: process.env.GITLAB_APP_ID,
          clientSecret: process.env.GITLAB_APP_SECRET,
          callbackURL: process.env.GITLAB_APP_CALLBACK,
        },
        identifyUser,
    );
  }
  if (process.env.NODE_ENV == 'test') {
    passport.use(buildMockStrategy());
  } else {
    passport.use(buildGitlabStrategy());
  }

  passport.serializeUser((gitUser, done) => {
    return done(null, gitUser.id);
  });
  passport.deserializeUser(async function(userId, done) {
    const gitlabProfile = await GitProfiles.findGitProfileByUserId(
        userId,
        'gitlab',
    );
    const accessToken = Crypto.decrypt(gitlabProfile.ACCESS_TOKEN);
    const refreshToken = Crypto.decrypt(gitlabProfile.REFRESH_TOKEN);
    const gitUser = {
      id: gitlabProfile.USER_ID,
      gitlab: {
        id: gitlabProfile.ID,
        access_token: accessToken,
        refresh_token: refreshToken,
      },
    };
    return done(null, gitUser);
  });

  return passport;
};
