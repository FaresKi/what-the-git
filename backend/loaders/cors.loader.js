const cors = require('cors');
module.exports = function(app) {
  app.use(cors({origin: process.env.FRONT_END_URL, credentials: true}));
};
