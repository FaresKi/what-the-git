const evt = require('../utils/environment');
const bodyParser = require('body-parser');
const session = require('express-session');

const options = {
  sessionSecret: evt('SESSION_SECRET'),
};

module.exports = function(app) {
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(
      session({
        secret: options.sessionSecret,
        resave: false,
        saveUninitialized: false,
        cookie: {path: '/', httpOnly: true, maxAge: null},
      }),
  );
};
