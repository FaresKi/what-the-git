/* eslint-disable require-jsdoc */
const http = require('http');

module.exports = function(port, app) {
  const server = http.createServer(app);
  server.listen(port);
  return server;
};
