const evt = require('../utils/environment');
const PostgresStore = require('../db/services/postgres-store');

const config = {
  host: evt('POSTGRES_HOST'),
  port: evt('POSTGRES_PORT'),
  user: evt('POSTGRES_USER'),
  password: evt('POSTGRES_PASSWORD'),
  database: evt('POSTGRES_DB'),
};

PostgresStore.init(config).then(
    () => {
      PostgresStore.client.query('SELECT \'DB_CONNECTED\' AS STATUS_DB')
          .then((r) => console.log(r.rows[0]));
    },
);

module.exports = PostgresStore;
