const cookieParser = require('cookie-parser');
const express = require('express');
const path = require('path');

module.exports = function() {
  const app = express();
  app.use(express.json());
  app.use(express.urlencoded({extended: false}));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  return app;
};
