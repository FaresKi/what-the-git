const options = {
  swaggerDefinition: {
    info: {
      description: 'WhatTheGit',
      title: 'Swagger',
      version: '1.0.0',
    },
    host: 'localhost:3000',
    basePath: '/',
    produces: [
      'application/json',
      'application/xml',
    ],
    schemes: ['https'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: '',
      },
    },
  },
  basedir: __dirname, // app absolute path
  files: ['../utils/response-builder.js', '../routes/*.js'],
};

module.exports = function(app) {
  const expressSwagger = require('express-swagger-generator')(app);
  expressSwagger(options);
};
