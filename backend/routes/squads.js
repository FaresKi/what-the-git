const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const ResponseBuilder = require('../utils/response-builder');
const Squads = require('../db/models/squads');
const AccessRights = require('../middlewares/access-rights');
const ResourcesExist = require('../middlewares/resources-exist');
const Users = require('../db/models/users');
/**
 * Returns the squad's informations
 * !!! Members' activities will be intially null for performance reasons !!!
 * @route GET /api/v1/squads/:squadId
 * @group SQUADS - Squads
 * @param {Number} squadId.path.required - Squad's ID
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @returns {Squad.model} 200 - Squad's infos
 * @returns {Error} 404 - Squad not found
 */
router.get(
  '/:squadId',
  ResourcesExist.squadExists,
  AccessRights.canViewSquad,
  async function (req, res) {
    const accessToken = req.user.gitlab.access_token;
    const squadId = req.params.squadId || null;
    try {
      const squad = await ResponseBuilder.buildSquadFromSquadId(
        accessToken,
        squadId,
      );
      res.json(squad);
    } catch (err) {
      console.log('GET /squads/:squadId ' + err);
      res.status(400).send('Bad request');
    }
  },
);

/**
 * Returns the squad's informations with the details of each member in the squad
 * @route GET /api/v1/squads/:squadId/details
 * @group SQUADS - Squads
 * @param {Number} squadId.path.required - Squad's ID
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @param {string} startdate.headers.required - start commit's date (yyyy-MM-dd)
 * @param {string} enddate.headers.required - end commit's date (yyyy-MM-dd)
 * @returns {Squad.model} 200 - Squad's infos
 * @returns {Error} 404 - Squad not found
 */
router.get(
  '/:squadId/details',
  ResourcesExist.squadExists,
  AccessRights.canViewSquad,
  async function (req, res) {
    const dataRegexExpression = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/
    const squadId = req.params.squadId;
    const startDate = req.headers.startdate;
    const endDate = req.headers.enddate;
    const accessToken = req.user.gitlab.access_token;
    if (!(startDate.match(dataRegexExpression) && endDate.match(dataRegexExpression))) {
      console.log('/api/v1/user/:profileId/activity ' + err);
      res.status(400).json({ 'error': 'Bad request' });
      return;
    }
    try {
      const squad = await ResponseBuilder.buildSquadFromSquadId(
        accessToken,
        squadId,
        startDate,
        endDate,
      );
      res.json(squad);
    } catch (err) {
      console.log('GET /squads/:squadId/details ' + err);
      res.status(400).send('Bad request');
    }
  },
);

/**
 * Creates a squad
 * @route POST /api/v1/squads
 * @group SQUADS - Squads
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @param {string} displayname.headers.required
 * @returns {Squad.model} 200 - Squad's infos
 * @returns {Error} 400 - Bad request
 */
router.post('/', async function (req, res) {
  const ownerId = req.user.id;
  const displayName = req.headers.displayname;
  const accessToken = req.user.gitlab.access_token;
  try {
    const tmp = await Squads.createSquad(ownerId, displayName);
    if (tmp == null) {
      res.status(400).json({ 'error': 'Bad request' });
    } else {
      const squad = await ResponseBuilder.buildSquadFromSquadId(
        accessToken,
        tmp.ID,
      );
      res.json(squad);
    }
  } catch (err) {
    console.log('POST /squads ' + err);
    res.status(400).send('Bad request');
  }
});

/**
 * Updates a squad
 * @route PUT /api/v1/squads/:squadId
 * @group SQUADS - Squads
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @param {string} pictureurl.headers
 * @param {string} displayname.headers.required
 * @param {string} membersids.headers.required JSON Array integers [id1,id2,...]
 * @returns {Squad.model} 200 - Squad's infos
 * @returns {Error} 400 - Bad request
 */
router.put(
  '/:squadId',
  ResourcesExist.squadExists,
  AccessRights.canEditSquad,
  async function (req, res) {
    const squadId = req.params.squadId;
    const pictureUrl = req.headers.pictureurl;
    const displayName = req.headers.displayname;
    const membersIDs = JSON.parse(req.headers.membersids || null);//TODO: improve parse and add try catch
    const ownerId = req.user.id;
    const accessToken = req.user.gitlab.access_token;
    try {
      for (const id of membersIDs) {
        const user = await Users.findUser(id);
        if (!user) {
          res.status(404).json({ 'error': 'Not found' });
          return;
        }
      }
      const tmp = await Squads.updateSquad(
        squadId,
        pictureUrl,
        displayName,
        membersIDs,
        ownerId,
      );
      if (!tmp) {
        res.status(400).send('Bad request');
        return;
      }
      const squad = await ResponseBuilder.buildSquadFromSquadId(
        accessToken,
        squadId,
      );
      res.json(squad);
    } catch (err) {
      console.log('PUT /squads/:squadId ' + err);
      res.status(400).send('Bad request');
    }
  },
);

/**
 * Deletes a squad
 * @route DELETE /api/v1/squads/:squadId
 * @group SQUADS - Squads
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @param {Number} squadId.path.required - Squad's ID
 * @returns {Squad.model} 200 - Squad's infos
 * @returns {Error} 400 - Bad request
 */
router.delete(
  '/:squadId',
  ResourcesExist.squadExists,
  AccessRights.canEditSquad,
  async function (req, res) {
    const accessToken = req.user.gitlab.access_token;
    const squadId = req.params.squadId;
    try {
      const squad = await ResponseBuilder.buildSquadFromSquadId(
        accessToken,
        squadId,
      );
      await Squads.deleteSquad(squadId);
      res.json(squad);
    } catch (err) {
      console.log('DELETE /squads/:squadId ' + err);
      res.status(400).send('Bad request');
    }
  },
);
module.exports = router;
