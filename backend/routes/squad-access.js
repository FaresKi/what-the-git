// @ts-nocheck
const express = require('express');
const ResponseBuilder = require('../utils/response-builder');
const SquadAccess = require('../db/models/squad-access');
const ResourcesExist = require('../middlewares/resources-exist');
const AccessRights = require('../middlewares/access-rights');
// eslint-disable-next-line new-cap
const router = express.Router();
/**
 * Check if the provided access is valid
 * @param {String} userAccess
 * @return {Boolean}
 */
function validUserAccess(userAccess) {
  return userAccess !== 'R' && userAccess !== 'RW';
}

/**
 * Return the squad's users access list
 * @route Get /api/v1/squads/:squadId/access
 * @group SQUADS ACCESS - Squads user's access
 * @param {Number} squadId.path.required - Squad's ID
 * @return {Array.<SquadAccess> | Error} 200 - Squad access's infos
 * 400 - Bad request
 * 404 - Squad not found
 */
router.get(
    '/:squadId/access',
    ResourcesExist.squadExists,
    AccessRights.canViewSquad,
    async function(req, res) {
      const squadId = req.params.squadId;
      try {
        const squadAccess = await ResponseBuilder.buildSquadAccessFromSquadId(
            // @ts-ignore
            squadId,
        );
        res.json(squadAccess);
      } catch (err) {
        console.log('GET squads/:squadId/access ' + err);
        res.status(400).send('Bad request');
      }
    },
);

/**
 * Deletes a squad access
 * @route Delete /api/v1/squads/:squadId/access/:userId
 * @group SQUADS ACCESS - Squads user's access
 * @param {Number} squadId.path.required - Squad's ID
 * @param {Number} userId.path.required - which user want to delete
 * @return {SquadAccess.model | Error} 200 - Squad access's infos
 *  400 - Bad request
 *  404 - Squad not found
 */
router.delete(
    '/:squadId/access/:userId',
    ResourcesExist.squadExists,
    ResourcesExist.userExists,
    AccessRights.canEditSquad,
    async function(req, res) {
      const squadId = req.params.squadId;
      const userId = req.params.userId;
      try {
        // @ts-ignore
        const tmp = await SquadAccess.deleteSquadAccess(squadId, userId);
        if (!tmp) {
          res.status(404).json('Not found');
          return;
        }
        const squadAccess = await ResponseBuilder.buildSquadAccessFromSquadId(
            // @ts-ignore
            squadId,
        );
        res.json(squadAccess);
      } catch (err) {
        console.log('DELETE /squads/:squadId/access/:userId ' + err);
        res.status(400).send('Bad request');
      }
    },
);

/**
 * Updates a squad access
 * @route PUT /api/v1/squads/:squadId/access/:userId
 * @group SQUADS ACCESS - Squads user's access
 * @param {Number} squadId.path.required - Squad's ID
 * @param {Number} userId.path.required - which user want to update
 * @param {String} userAccess.path.required - user's new access(R or RW)
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @return {SquadAccess.model | Error} 200 - Squad access's infos
 * 400 - Bad request
 * 404 - Squad not found
 */
router.put(
    '/:squadId/access/:userId',
    ResourcesExist.squadExists,
    ResourcesExist.userExists,
    AccessRights.canEditSquad,
    async function(req, res) {
      // @ts-ignore
      const ownerId = req.user.id;
      const squadId = req.params.squadId;
      const userId = req.params.userId;
      const userAccess = req.headers.useraccess;

      // @ts-ignore
      if (validUserAccess(userAccess)) {
        res.status(400).json({'error': 'Bad request'});
        return;
      }
      if (userId == ownerId) {
        res.status(400).json({'error': 'Bad request'});
      }

      try {
        const tmp = await SquadAccess.updateSquadAccess(
            // @ts-ignore
            squadId,
            userId,
            userAccess,
        );
        if (!tmp) {
          res.status(404).json('Not found');
          return;
        }
        const squadAccess = await ResponseBuilder.buildSquadAccessFromSquadId(
            // @ts-ignore
            squadId,
        );
        res.json(squadAccess);
      } catch (err) {
        console.log('PUT /squads/:squadId/access/:userId ' + err);
        res.status(400).json({'error': 'Bad request'});
      }
    },
);

/**
 * Adds a new squad access
 * @route Post /api/v1/squads/:squadId/access/:userId
 * @group SQUADS ACCESS - Squads user's access
 * @param {Number} squadId.path.required - Squad's ID
 * @param {Number} userId.path.required - which user want to give access
 * @param {String} userAccess.path.required - user's new access(R or RW)
 * @return {SquadAccess.model | Error} 200 - Squad access's infos
 *  400 - Bad request
 *  404 - Squad not found
 */
router.post(
    '/:squadId/access/:userId',
    ResourcesExist.squadExists,
    ResourcesExist.userExists,
    AccessRights.canEditSquad,
    async function(req, res) {
      const squadId = req.params.squadId;
      const userId = req.params.userId;
      const userAccess = req.headers.useraccess;
      // @ts-ignore
      if (validUserAccess(userAccess)) {
        res.status(400).json({'error': 'Bad request'});
        return;
      }
      try {
        const addedSquadAccess = await SquadAccess.createSquadAccess(
            // @ts-ignore
            squadId,
            userId,
            userAccess,
        );
        if (addedSquadAccess == null) {
          res.status(400).json({'error': 'Bad request'});
          return;
        }
        const squadAccess = await ResponseBuilder.buildSquadAccessFromSquadId(
            // @ts-ignore
            squadId,
        );
        if (squadAccess === null) {
          res.status(400).json({'error': 'Bad request'});
          return;
        }
        res.json(squadAccess);
      } catch (err) {
        console.log('POST /:squadId/access/:userId ' + err);
        res.status(400).json({'error': 'Bad request'});
      }
    },
);
module.exports = router;
