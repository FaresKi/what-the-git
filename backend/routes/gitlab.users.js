const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const ResponseBuilder = require('../utils/response-builder');

/**
 * Returns the Gitlab profile's id activity
 * @route GET /api/v1/users/:profileId/activity
 * @group USERS - Gitlab users
 * @param {Number} profileId.path.required - Gitlab profile's id
 * @param {User} user.query.required - User's infos (alredy in cookie)
 * @param {string} startdate.headers.required - start commit's date (yyyy-MM-dd)
 * @param {string} enddate.headers.required - end commit's date (yyyy-MM-dd)
 * @returns {Activity.model} 200 - Activity infos
 * @returns {Error} 404 - Not found
 */
router.get('/:profileId/activity', async function(req, res) {
  try {
    const profileId = req.params.profileId.replace(/[^a-zA-Z0-9]/g, "");
    if (!parseInt(profileId, 10)) {
      res.status(400).json({'error':'Bad request'});
    }
    const dataRegexExpression = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/
    const accessToken = req.user.gitlab.access_token;
    const startDate = req.headers.startdate;
    const endDate = req.headers.enddate;
    if(!(startDate.match(dataRegexExpression) && endDate.match(dataRegexExpression)))
    {
      console.log('/api/v1/user/:profileId/activity ' + err );
      res.status(400).json({'error':'Bad request'});
      return;
    }
    const activity = await ResponseBuilder.buildActivityFromProfileId(
        accessToken,
        startDate,
        endDate,
        profileId,
        false,
    );
    if (activity) {
      res.json(activity);
    } else {
      res.status(404).json({'error':'Not found'});
    }
  } catch (err) {
    console.log('/api/v1/user/:profileId/activity ' + err );
    res.status(400).json({'error':'Bad request'});
  }
});

module.exports = router;
