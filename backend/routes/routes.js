module.exports = function(app, passport) {
  const authRouter = require('./auth')(passport);
  const squadsRouter = require('./squads');
  const squadAccessRouter = require('./squad-access');
  const contributorsRouter = require('./contributors');
  const gitlabUsersRouter = require('./gitlab.users');
  const isConnected = require('../middlewares/is-connected');

  app.use('/auth', authRouter);
  app.use('/api/v1/users', isConnected, gitlabUsersRouter);
  app.use('/api/v1/squads', isConnected, squadsRouter);
  app.use('/api/v1/squads', isConnected, squadAccessRouter);
  app.use('/api/v1', isConnected, contributorsRouter);
};
