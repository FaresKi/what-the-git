const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const ResponseBuilder = require('../utils/response-builder');

/**
 * Get Gitlab contributors
 * @route GET /api/v1/gitlab/contributors
 * @group GITLAB - Gitlab
 * @param {User} user.query.required - User's infos
 * @returns {Array.<SquadMember>} 200 - Contributors' infos
 * @returns {Error} 400 - Bad request
 */
router.get('/gitlab/contributors', async function(req, res) {
  const accessToken = req.user.gitlab.access_token;
  const ctrb = await ResponseBuilder.buildGitlabContributorsFromAccessToken(
      accessToken,
  );
  if (ctrb !== null) {
    res.json(ctrb);
  } else {
    res.status(400).json({'error':'Bad request'});
  }
});

module.exports = router;
