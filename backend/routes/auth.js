const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const ResponseBuilder = require('../utils/response-builder');
const isConnected = require('../middlewares/is-connected');

module.exports = function(passport) {
  /**
   * @route GET /auth/logout
   * @group AUTH - Authentication
   */
  router.get('/logout', isConnected, function(req, res, next) {
    req.session.destroy();
    req.logout();
    res.status(200).send('Logged out');
  });
  /**
   * @route GET /auth/error
   * @group AUTH - Authentication
   * @returns {Error} 400 - Unexpected error
   */
  router.get('/error', function(req, res, next) {
    res.status(400).json({ 'error': 'Bad request' });
  });

  /**
   * Gitlab OAUTH Authentication
   * @route GET /auth/gitlab
   * @group AUTH - Authentication
   */
  router.get(
      '/gitlab',
      passport.authenticate('gitlab', {
        failureRedirect: '/error',
        scope: ['api'],
        successRedirect: '/auth/me',
      }),
  );

  router.get(
      '/gitlab/callback',
      passport.authenticate('gitlab', {
        failureRedirect: '/error',
        scope: ['api'],
      }),
      function(req, res, next) {
        res.send('<script>window.close()</script>');
      },
  );

  /**
   * @route GET /auth/me
   * @group AUTH - Authentication
   * @returns {User.model} 200 - User's info
   * @returns {Error} 400 - Bad request
   */
  router.get('/me', isConnected, async function(req, res, next) {
    try {
      const user = await ResponseBuilder.buildUserFromUserId(
          req.user.gitlab.access_token,
          req.user.id,
      );
      res.json(user);
    } catch (err) {
      console.log('/auth/me err ' + err);
      res.status(400).send('Bad request');
    }
  });

  return router;
};
