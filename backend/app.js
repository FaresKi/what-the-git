require('dotenv').config();
const app = require('./loaders/express.loader')();
require('./loaders/cors.loader')(app);
require('./loaders/session.loader')(app);
require('./loaders/http.loader')(process.env.APP_PORT || 3000, app);
require('./loaders/swagger.loader')(app);
const passport = require('./loaders/passport.loader')(app);
require('./loaders/postgres.loader');
require('./routes/routes')(app, passport);
