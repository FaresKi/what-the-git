/* eslint-disable require-jsdoc */
const SquadAccess = require('../db/models/squad-access');


async function canViewSquad(req, res, next) {
  try {
    const squadId = req.params.squadId.replace(/[^a-zA-Z0-9]/g, "")
    if (parseInt(squadId, 10)) {
      const userId = req.user.id;

      const access = await SquadAccess.findSquadAccessByUserId(squadId, userId);
      if (access != null) {
        next();
        return;
      } else {
        res.status(403).send('Refused Access');
      }
    }
    else {
      res.status(400).send({ error: 'Bad request' });
    }
  } catch (err) {
    console.log('canViewSquad ' + err);
    res.status(400).send('Bad request');
  }
}

async function canEditSquad(req, res, next) {
  try {
    const squadId = req.params.squadId.replace(/[^a-zA-Z0-9]/g, "")
    if (parseInt(squadId, 10)) {
      const userId = req.user.id;

      const access = await SquadAccess.findSquadAccessByUserId(squadId, userId);
      if (access != null) {
        if (access.ACCESS_TYPE === 'RW') {
          next();
          return;
        }
      }
      res.status(403).send('Refused Access');
    }
    else {
      res.status(400).send({ error: 'Bad request' });
    }
  } catch (err) {
    console.log('canViewSquad ' + err);
    res.status(400).send('Bad request');
  }
}

module.exports = {
  canViewSquad: canViewSquad,
  canEditSquad: canEditSquad,
};
