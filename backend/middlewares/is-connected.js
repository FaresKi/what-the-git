/* eslint-disable require-jsdoc */
function isConnected(req, res, next) {
  if (req.user) {
    next();
    return;
  }
  res.status(401).send({error: 'Not authenticated'});
}

module.exports = isConnected;
