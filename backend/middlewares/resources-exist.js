/* eslint-disable require-jsdoc */
const Squads = require('../db/models/squads');
const Users = require('../db/models/users');

async function squadExists(req, res, next) {
  const squadId = req.params.squadId.replace(/[^a-zA-Z0-9]/g, "")
  if(parseInt(squadId, 10)){
  const squad = await Squads.findSquad(squadId || null);
    if (squad) {
      next();
      return;
    }
    else{
    res.status(404).json({'error': 'Not found'});
    }
  }
  else{
    res.status(400).json({'error': 'Bad request'});
  }
}

async function userExists(req, res, next) {
  const userId = req.params.userId.replace(/[^a-zA-Z0-9]/g, "")
  if(parseInt(userId, 10)){
    const user = await Users.findUser(userId || null);
    if (user) {
      next();
      return;
    }
    else{
      res.status(404).json({'error': 'Not found'});
    }
  }
  else{
    res.status(400).json({'error': 'Bad request'});
  }
  
}

module.exports = {
  squadExists: squadExists,
  userExists: userExists,
};
