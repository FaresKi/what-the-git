const testTools = require('../utils/test-tools');
const mockedProjects = require('../mocked-objects/gitlab/mocked-projects');
const expect = testTools.expect;
const nock = require('nock');

describe('GET /api/v1/gitlab/contributors', async () => {
  it('should return an empty array if user doesn\'nt have access to projects',
      async () => {
        await testTools.callContributorsApiAfterMockingGitlab([],
            async (agent, response) => {
              expect(response.statusCode)
                  .to.equal(200);
              expect(JSON.parse(response.text))
                  .to.be.an('array')
                  .that.has.lengthOf(0);
            });
      });
  it('should return the correct details of all existing contributors of one specific project',
      async () => {
        const project1 = mockedProjects.projects[0];
        await testTools.callContributorsApiAfterMockingGitlab([project1],
            async (agent, members) => {
              expect(members.statusCode)
                  .to.equal(200);
              const membersObj = JSON.parse(members.text);

              const fakeMembers = [];
              for (const member of project1.members) {
                fakeMembers.push({
                  profile_id: member.id,
                  username: member.username,
                  name: member.name,
                  avatar_url: member.avatar_url,
                  web_url: member.web_url,
                },
                );
              }
              expect(membersObj)
                  .to.be.an('array')
                  .that.has.lengthOf(fakeMembers.length);
              membersObj.forEach((member) => {
                const fm = fakeMembers
                    .find((fm) => fm.profile_id === member.gitlab.profile_id);
                expect(member)
                    .to.has.keys('user_id', 'gitlab', 'activity');
                expect(member.user_id)
                    .to.be.a('number')
                    .and.to.be.greaterThan(0);

                expect(member.gitlab)
                    .that.eql(fm);

                expect(member.activity)
                    .to.be.null;
              });
            });
      });
  it('should return contributors list of 2 or more projects without duplication'
      , async () => {
        await testTools.callContributorsApiAfterMockingGitlab(
            mockedProjects.projects
            , async (agent, members) => {
              expect(members.statusCode)
                  .to.equal(200);
              const membersObj = JSON.parse(members.text);
              const fakeMembersId = new Set();
              for (const project of mockedProjects.projects) {
                for (const member of project.members) {
                  fakeMembersId.add(member.id);
                }
              }
              expect(membersObj)
                  .to.be.an('array')
                  .that.has.lengthOf(fakeMembersId.size);
              for (const member of membersObj) {
                const obj = fakeMembersId.has(member.gitlab.profile_id);
                expect(obj)
                    .to.not.be.null;
                fakeMembersId.delete(member.gitlab.profile_id);
              }
            });
      });
  it('should return 400 error if it doesn\'t receive a correct json object'
      , async () => {
        await testTools.callContributorsApiAfterMockingGitlab(['{test']
            , async (agent, response) => {
              await testTools.check400BadRequest(async (agent) => {
                return response;
              });
            });
      });
  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/api/v1/gitlab/contributors');
    });
    await nock.cleanAll();
  });
});
