const testTools = require('../utils/test-tools');
const expect = testTools.expect;
const mockedProfile = require('../mocked-objects/gitlab/mocked-profile');

before(async function() {
  await testTools.initDB();
  await testTools.cleanDB();
});

after(function() {
  testTools.closeDbConnection();
});

describe('GET /auth/me', async () => {
  it('should return profile if agent has credentials', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);
    const response = await agent.get('/auth/me')
        .expect(200);
    expect(response.body)
        .to.be.an('object');

    const returnedObj = JSON.parse(response.text);

    expect(returnedObj)
        .has.keys('gitlab', 'squads', 'user_id');

    expect(returnedObj.user_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

    expect(returnedObj.gitlab)
        .to.eql(
            {
              profile_id: mockedProfile['id'],
              name: mockedProfile['name'],
              username: mockedProfile['username'],
              avatar_url: mockedProfile['avatar_url'],
              web_url: mockedProfile['web_url'],
            });

    expect(returnedObj.squads)
        .to.be.an('array')
        .that.has.lengthOf(0);
  });
  it('should return 401 if agent dosen\'t have credentials', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/auth/me');
    });
  });
});

describe('GET /auth/error', async () => {
  it('should return 400', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return agent.get('/auth/error');
    });
  });
});

describe('GET /auth/logout', async () => {
  it('should return 200 and logout if user has a cookie', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const response = await agent.get('/auth/logout')
        .expect(200);
    expect(response.text)
        .is.equal('Logged out');
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/auth/logout');
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/auth/logout');
    });
  });
});
