const nock = require('nock');
const testTools = require('../utils/test-tools');
const mockedProjects = require('../mocked-objects/gitlab/mocked-projects');
const mockedUsers = require('../mocked-objects/gitlab/mocked-users');
const expect = testTools.expect;

describe('GET /api/v1/users/:profileId/activity', async () => {
  it('should return the activity of specefic profileId', async () => {
    await testTools.callContributorsApiAfterMockingGitlab(mockedProjects.projects
        , async (agent, response) => {
          await testTools.mockUserProfile(mockedUsers.users
              , '2021-01-07', '2020-12-14');
          const members = JSON.parse(response.text);
          const member = members.find((m) => m.gitlab.profile_id === 3743761);
          const activityResponse = await agent.get(`/api/v1/users/${member.gitlab.profile_id}/activity`)
              .set({
                startdate: '2020-12-15',
                enddate: '2021-01-06',
              })
              .expect(200);
          const activity = JSON.parse(activityResponse.text);
          expect(activity)
              .to.be.an('object')
              .to.has.keys('languages', 'last_commit', 'commits');

          expect(activity.languages)
              .to.be.an('object');
          let total = 0;
          for (const k in activity.languages) {
            total += activity.languages[k];
          }
          expect(total)
              .to.be.greaterThan(99)
              .and.to.be.lessThan(100);

          expect(activity.last_commit)
              .to.be.an('object')
              .that.has.keys('commit_id', 'project_id', 'project_name', 'project_url', 'branch', 'title', 'committed_date');

          expect(activity.commits)
              .to.be.an('array');

          expect(activity.commits[0])
              .to.eql(activity.last_commit);
        });
  });

  it('should return 404 error if profile doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return await agent.get(`/api/v1/users/123/activity`)
          .set({
            startdate: '2020-12-15',
            enddate: '2021-01-06',
          });
    });
  });

  it('should return 400 error if startdate isn\'t correct', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const userObj = JSON.parse(user.text);
      return agent.get('/api/v1/users/' + userObj.user_id + '/activity')
          .set({
            startdate: '2020-12-15--',
            enddate: '2021-01-06',
          });
    });
  });

  it('should return 400 error if endate isn\'t correct', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const userObj = JSON.parse(user.text);
      return agent.get('/api/v1/users/' + userObj.user_id + '/activity')
          .set({
            startdate: '2020-12-15',
            enddate: '2021-01',
          });
    });
  });

  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return agent.get('/api/v1/users/test/activity');
    });
  });

  it('should return 401 error if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/api/v1/users/1234/activity');
    });
    await nock.cleanAll();
  });
});
