const testTools = require('../utils/test-tools');
const expect = testTools.expect;
let newUserId = 1000;

// eslint-disable-next-line require-jsdoc
async function createUserAndGiveItAccesstoSquad(userAccess, cb) {
  const newUser = await testTools.addNewUser(newUserId++);
  const agent = await testTools.createAgent();
  await agent.get('/auth/gitlab')
      .withCredentials()
      .expect(302);

  const user = await agent.get('/auth/me')
      .expect(200);

  const userObj = JSON.parse(user.text);
  const squadObj = userObj.squads[0];

  const response = await agent.post(`/api/v1/squads/${squadObj.squad_id}
    /access/${newUser.USER_ID}`)
      .set({useraccess: userAccess})
      .expect(200);
  const toDelete = await cb(agent, response, newUser, userObj, squadObj);
  if (toDelete) {
    await agent.delete(`/api/v1/squads/${squadObj.squad_id}
      /access/${newUser.USER_ID}`)
        .set({useraccess: userAccess})
        .expect(200);
  }
}


describe('GET /api/v1/squads/:squadId/access', async () => {
  it('should return all squad access', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const user = await agent.get('/auth/me')
        .expect(200);

    const userObj = JSON.parse(user.text);

    const displayName = 'test squad';

    const response = await agent.post('/api/v1/squads/')
        .set({displayname: displayName})
        .expect(200);

    const returnedSquad = JSON.parse(response.text);

    const response2 = await agent.get(`/api/v1/squads/
    ${returnedSquad.squad_id}/access`);

    const returnedAccess = JSON.parse(response2.text);

    expect(returnedAccess)
        .to.be.an('array')
        .that.has.lengthOf(1)
        .eql([{
          user_id: userObj.user_id,
          access_type: 'RW',
        }]);
  });

  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return await agent.get(`/api/v1/squads/${squadId}/access`);
    });
  });

  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return await agent.get('/api/v1/squads/test/access');
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/api/v1/squads/test/access');
    });
  });
});

describe('POST /api/v1/squads/:squadId/access/:userId', async () => {
  it('should add user to the list of squad-access', async () => {
    const userAccess = 'R';
    await createUserAndGiveItAccesstoSquad(userAccess
        , async (agent, response, newUser, userObj, squadObj) => {
          const squadAccess = JSON.parse(response.text);
          expect(squadAccess)
              .to.be.an('array')
              .that.has.lengthOf(2)
              .and.has.eql([
                {
                  'user_id': userObj.user_id,
                  'access_type': 'RW',
                }, {
                  'user_id': newUser.USER_ID,
                  'access_type': userAccess,
                }]);
          return true;
        });
  });

  it('should return 400 error if user already has access', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const userAccess = 'RW';
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      return await agent.post(`/api/v1/squads/${squadObj.squad_id}
          /access/${userObj.user_id}`)
          .set({useraccess: userAccess})
          .expect(400);
    });
  });

  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const newUser = await testTools.addNewUser(newUserId++);
      return await agent.post(`/api/v1/squads/${squadId}/access/${newUser.USER_ID}`);
    });
  });

  it('should return 404 if user doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      const userAccess = 'R';
      return await agent.post(`/api/v1/squads/${squadObj.squad_id}/access/1234`)
          .set({useraccess: userAccess});
    });
  });

  it('should return 400 userAccess isn\'t R or RW', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const newUser = await testTools.addNewUser(newUserId++);
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      const userAccess = 'RR';
      return await agent.post(`/api/v1/squads/${squadObj.squad_id}
      /access/${newUser.USER_ID}`)
          .set({useraccess: userAccess})
          .expect(400);
    });
  });

  it('should return 400 error if squadId or userId aren\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return await agent.post('/api/v1/squads/test/access/1');
    });
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const squadObj = JSON.parse(user.text).squads[0];
      return await agent.post(`/api/v1/squads/${squadObj.squad_id}/access/test`);
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.post('/api/v1/squads/1/access/1');
    });
  });
});


describe('PUT /api/v1/squads/:squadId/access/:userId', async () => {
  it('should modify the userAccess of a specific squad', async () => {
    let userAccess = 'R';
    await createUserAndGiveItAccesstoSquad(userAccess
        , async (agent, response, newUser, userObj, squadObj) => {
          userAccess = 'RW';
          const putResponse = await agent.put(`/api/v1/squads/${squadObj.squad_id}/access/${newUser.USER_ID}`)
              .set({useraccess: userAccess})
              .expect(200);

          const squadAccess = JSON.parse(putResponse.text);

          expect(squadAccess)
              .to.be.an('array')
              .that.has.lengthOf(2)
              .and.has.eql([
                {
                  'user_id': userObj.user_id,
                  'access_type': 'RW',
                }, {
                  'user_id': newUser.USER_ID,
                  'access_type': userAccess,
                }]);
          return true;
        });
  });
  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const newUser = await testTools.addNewUser(newUserId++);
      return await agent.put(`/api/v1/squads/${squadId}/access/${newUser.USER_ID}`)
          .set({useraccess: 'R'});
    });
  });

  it('should return 404 if user doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      const userAccess = 'R';
      return await agent.put(`/api/v1/squads/${squadObj.squad_id}/access/1234`)
          .set({useraccess: userAccess});
    });
  });

  it('should return 400 if userAccess isn\'t R or RW', async () => {
    let userAccess = 'R';
    await createUserAndGiveItAccesstoSquad(userAccess
        , async (agent, response, newUser, userObj, squadObj) => {
          userAccess = 'RR';
          await testTools.check400BadRequest(async (agent) => {
            return await agent.put(`/api/v1/squads/${squadObj.squad_id}
      /access/${newUser.USER_ID}`)
                .set({useraccess: userAccess});
          });
          return true;
        });
  });

  it('should return 400 if userId is the ownerId', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      const userAccess = 'R';

      return await agent.put(`/api/v1/squads/${squadObj.squad_id}
      /access/${user.user_id}`)
          .set({useraccess: userAccess})
          .expect(400);
    });
  });

  it('should return 400 error if squadId or userId aren\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return await agent.put('/api/v1/squads/test/access/1');
    });
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const squadObj = JSON.parse(user.text).squads[0];
      return await agent.put(`/api/v1/squads/${squadObj.squad_id}/access/test`);
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.put('/api/v1/squads/1/access/1');
    });
  });
});

describe('DELETE /api/v1/squads/:squadId/access/:userId', async () => {
  it('should delete the userAccess of a specific squad', async () => {
    const userAccess = 'R';
    await createUserAndGiveItAccesstoSquad(userAccess
        , async (agent, response, newUser, userObj, squadObj) => {
          const deleteResponse = await agent.delete(`/api/v1/squads/${squadObj.squad_id}
    /access/${newUser.USER_ID}`)
              .expect(200);

          const squadAccess = JSON.parse(deleteResponse.text);

          expect(squadAccess)
              .to.be.an('array')
              .that.has.lengthOf(1)
              .and.has.eql([
                {
                  'user_id': userObj.user_id,
                  'access_type': 'RW',
                }]);
          return false;
        });
  });
  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const newUser = await testTools.addNewUser(newUserId++);
      return await agent.delete(`/api/v1/squads/${squadId}/access/${newUser.USER_ID}`);
    });
  });

  it('should return 404 if user doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];
      return await agent.delete(`/api/v1/squads/${squadObj.squad_id}/access/1234`);
    });
  });

  it('should return 400 if userId is the ownerId', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];

      return await agent.delete(`/api/v1/squads/${squadObj.squad_id}
      /access/${user.user_id}`);
    });
  });

  it('should return 400 error if squadId or userId aren\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return await agent.delete('/api/v1/squads/test/access/1');
    });
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const squadObj = JSON.parse(user.text).squads[0];
      return await agent.delete(`/api/v1/squads/${squadObj.squad_id}/access/test`);
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.delete('/api/v1/squads/1/access/1');
    });
  });
});
