const testTools = require('../utils/test-tools');
const mockedProjects = require('../mocked-objects/gitlab/mocked-projects');
const mockedUsers = require('../mocked-objects/gitlab/mocked-users');
const expect = testTools.expect;
const nock = require('nock');

describe('POST /api/v1/squads/', async () => {
  it('should create new squad and return it', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const user = await agent.get('/auth/me')
        .expect(200);

    const userObj = JSON.parse(user.text);

    const displayName = 'test squad';

    const response = await agent.post('/api/v1/squads/')
        .set({displayname: displayName})
        .expect(200);

    const returnedSquad = JSON.parse(response.text);

    expect(returnedSquad)
        .has.keys('squad_id', 'picture_url', 'name', 'owner', 'members');

    expect(returnedSquad.squad_id)
        .to.be.a('number')
        .and.to.be.greaterThan(0);

    expect(returnedSquad.picture_url)
        .to.be.null;

    expect(returnedSquad.name)
        .to.be.a('string')
        .and.equals(displayName);

    expect(returnedSquad.owner)
        .to.be.an('object')
        .and.has.keys('activity', 'gitlab', 'user_id');

    expect(returnedSquad.owner.activity)
        .to.be.null;

    expect(returnedSquad.owner.gitlab)
        .to.be.an('object')
        .and.eqls(userObj.gitlab);

    expect(returnedSquad.owner.user_id)
        .to.equal(userObj.user_id);

    expect(returnedSquad.members)
        .to.be.an('array')
        .that.has.lengthOf(1)
        .and.contains.something.like(returnedSquad.owner);
  });
  it('should return 400 if headers doesn\'t contain displayname', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return agent.post('/api/v1/squads/');
    });
  });

  it('should parse displayname if it contains a sql injection', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const name = '"(select "ACCESS_TOKEN" from "wtg"."GIT_PROFILES" limit 1)"';

    const response = await agent.post('/api/v1/squads/')
        .set('displayname', name)
        .expect(200);

    const returnedSquad = JSON.parse(response.text);

    expect(returnedSquad.name)
        .to.equal(name.replace('"', '\"'));
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.post('/api/v1/squads/');
    });
  });
});

describe('GET /api/v1/squads/:squadId', async () => {
  it('should return the right squad', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const user = await agent.get('/auth/me')
        .expect(200);

    const squadObj = JSON.parse(user.text).squads[0];

    const squad = await agent.get('/api/v1/squads/' + squadObj.squad_id)
        .expect(200);

    const returnedSquad = JSON.parse(squad.text);

    expect(returnedSquad)
        .to.be.an('object')
        .that.eql(squadObj);
  });

  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return agent.get('/api/v1/squads/' + squadId);
    });
  });

  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return agent.get('/api/v1/squads/test');
    });
  });

  it('should return 401 error if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/api/v1/squads/1');
    });
  });
});

describe('PUT /api/v1/squads/:squadId', async () => {
  it('should update squad if exist', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const pictureUrl = 'this.is/a/test';
    const displayName = 'new name';

    const user = await agent.get('/auth/me')
        .expect(200);

    const userObj = JSON.parse(user.text);
    const squadObj = userObj.squads[0];
    const squadObj2 = userObj.squads[1];

    const response = await agent.put('/api/v1/squads/' + squadObj.squad_id)
        .set({
          pictureUrl: pictureUrl,
          displayname: displayName,
          membersids: '[' + userObj.user_id + ']',
        })
        .expect(200);

    const returnedSquad = JSON.parse(response.text);

    squadObj.picture_url = pictureUrl;
    squadObj.name = displayName;
    expect(returnedSquad)
        .to.be.an('object')
        .that.eql(squadObj);

    const response2 = await agent.get('/api/v1/squads/' + squadObj2.squad_id)
        .expect(200);

    expect(JSON.parse(response2.text))
        .to.eql(squadObj2);
  });

  it('should return 404 if one of new members dosen\'t exist', async () => {
    testTools.check404NotFound(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);

      const userObj = JSON.parse(user.text);
      const squadObj = userObj.squads[0];

      return await agent.put('/api/v1/squads/' + squadObj.squad_id)
          .set({
            displayname: squadObj.name,
            membersids: '[' + [userObj.user_id, userObj.user_id + 1] + ']',
          });
    });
  });

  it('should return 404 if squad dosen\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return await agent.put('/api/v1/squads/' + squadId)
          .set({
            displayname: 'test',
            membersids: '[1]',
          });
    });
  });

  it('should parse displayname and pictureurl if it contains a sql injection'
      , async () => {
        const agent = await testTools.createAgent();
        await agent.get('/auth/gitlab')
            .withCredentials()
            .expect(302);

        const name = '"(select "ACCESS_TOKEN" from "wtg"."GIT_PROFILES" limit 1)"';

        const user = await agent.get('/auth/me')
            .expect(200);

        const userObj = JSON.parse(user.text);
        const squadObj = userObj.squads[0];

        const response = await agent.put('/api/v1/squads/' + squadObj.squad_id)
            .set({
              pictureurl: name,
              displayname: name,
              membersids: '[' + userObj.user_id + ']',
            })
            .expect(200);

        const returnedSquad = JSON.parse(response.text);

        expect(returnedSquad.name)
            .to.equal(name.replace('"', '\"'));

        expect(returnedSquad.name)
            .to.equal(name.replace('"', '\"'));
      });

  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const userObj = JSON.parse(user.text);

      return await agent.put('/api/v1/squads/test')
          .set({
            displayname: 'test',
            membersids: '[' + userObj.user_id + ']',
          });
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.put('/api/v1/squads/1');
    });
  });
});

describe('DELETE /api/v1/squads/:squadId', async () => {
  it('should delete the right squad', async () => {
    const agent = await testTools.createAgent();
    await agent.get('/auth/gitlab')
        .withCredentials()
        .expect(302);

    const user = await agent.get('/auth/me')
        .expect(200);

    const squadObj = JSON.parse(user.text).squads[0];

    const squad = await agent.delete('/api/v1/squads/' + squadObj.squad_id)
        .expect(200);

    const returnedSquad = JSON.parse(squad.text);

    expect(returnedSquad)
        .to.be.an('object')
        .that.eql(squadObj);

    await agent.delete('/api/v1/squads/' + squadObj.squad_id)
        .expect(404);
  });
  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return await agent.delete('/api/v1/squads/' + squadId);
    });
  });

  it('should return 401 if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return await agent.delete('/api/v1/squads/1');
    });
  });
  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return await agent.delete('/api/v1/squads/test');
    });
  });
});

describe('GET /api/v1/squads/:squadId/details', async () => {
  it('should return the details of all members of squads', async () => {
    await testTools.callContributorsApiAfterMockingGitlab(mockedProjects.projects
        , async (agent, members) => {
          const displayName = 'squadTest';
          expect(members.statusCode)
              .to.equal(200);
          await testTools.mockUserProfile(mockedUsers.users, '2021-01-07', '2020-12-14');
          const membersObj = JSON.parse(members.text);
          const mockedMember = membersObj.find((m) => m.gitlab.profile_id === 3743761);
          const squadResponse = await agent.post('/api/v1/squads/')
              .set({displayname: displayName})
              .expect(200);

          const squadObj = JSON.parse(squadResponse.text);
          await agent.put('/api/v1/squads/' + squadObj.squad_id)
              .set({
                displayname: squadObj.name,
                membersids: `[${squadObj.members[0].user_id}, ${mockedMember.user_id}]`,
              })
              .expect(200);
          const detailsResponse = await agent.get(`/api/v1/squads/${squadObj.squad_id}/details`)
              .set({
                startdate: '2020-12-15',
                enddate: '2021-01-06',
              })
              .expect(200);
          const squadActivity = JSON.parse(detailsResponse.text);
          expect(squadActivity)
              .has.keys('squad_id', 'picture_url', 'name', 'owner', 'members');

          expect(squadActivity.squad_id)
              .to.be.a('number')
              .and.to.be.greaterThan(0);

          expect(squadActivity.picture_url)
              .to.be.null;

          expect(squadActivity.name)
              .to.be.a('string')
              .and.equals(displayName);

          expect(squadActivity.owner)
              .to.be.an('object')
              .and.has.keys('activity', 'gitlab', 'user_id');

          expect(squadActivity.owner.activity)
              .to.be.null;

          expect(squadActivity.owner.gitlab)
              .to.be.an('object')
              .and.to.eql(squadObj.owner.gitlab);

          expect(squadActivity.owner.user_id)
              .to.equal(squadObj.members[0].user_id);

          expect(squadActivity.members)
              .to.be.an('array')
              .that.has.lengthOf(2);

          expect(squadActivity.members[0])
              .to.be.an('object')
              .that.eql(squadObj.members[0]);

          expect(squadActivity.members[1])
              .to.be.an('object')
              .that.is.not.null
              .and.has.keys('languages', 'last_commit', 'commits');


          expect(squadActivity.members[1].activity.commits)
              .to.be.an('array')
              .that.has.lengthOf(mockedUsers.users[1].commits.length);

          expect(squadActivity.members[1].activity.last_commit)
              .to.be.an('object')
              .that.eql(squadActivity.members[1].activity.commits[0]);

          const userLanguages = {};
          let projectNB = 0;
          for (const project of mockedProjects.projects) {
            const languages = project.languages;
            if (Object.keys(languages).length == 0) {
              continue;
            }
            projectNB++;
            Object.keys(languages).forEach((key) => {
              userLanguages[key] = (userLanguages[key] || 0) + languages[key];
            });
          }
          Object.keys(userLanguages).forEach((key) => {
            userLanguages[key] /= projectNB;
          });

          expect(squadActivity.members[1].activity.languages)
              .to.be.an('object')
              .that.eql(userLanguages);
        });
  });
  it('should return 404 error if squad doesn\'t exist', async () => {
    await testTools.shouldReturn404IfSquadNotExist(async (agent, squadId) => {
      return await agent.get('/api/v1/squads/' + squadId + '/details');
    });
  });

  it('should return 400 error if squadId isn\'t int', async () => {
    await testTools.check400BadRequest(async (agent) => {
      return agent.get('/api/v1/squads/test/details');
    });
  });

  it('should return 400 error if startdate isn\'t correct', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const userObj = JSON.parse(user.text);
      return agent.get('/api/v1/squads/' + userObj.squads[0] + '/details')
          .set({
            startdate: '2020-12-15--',
            enddate: '2021-01-06',
          });
    });
  });

  it('should return 400 error if endate isn\'t correct', async () => {
    await testTools.check400BadRequest(async (agent) => {
      const user = await agent.get('/auth/me')
          .expect(200);
      const userObj = JSON.parse(user.text);
      return agent.get('/api/v1/squads/' + userObj.squads[0] + '/details')
          .set({
            startdate: '2020-12-15',
            enddate: '2021-01',
          });
    });
  });

  it('should return 401 error if user doesn\'t have a cookie', async () => {
    await testTools.check401NotAuthenticated(async (agent) => {
      return agent.get('/api/v1/squads/1234/details');
    });
    nock.cleanAll();
  });
});
