/* eslint-disable max-len */
require('dotenv').config({path: __dirname + '/../../.env'});
const request = require('supertest');
const express = require('express');
// const {response} = require('express');
const session = require('express-session');
const evt = require('../../utils/environment');
const GitProfiles = require('../../db/models/git-profiles');
const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-like'));
chai.use(require('chai-things'));
const nock = require('nock');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(session({
  secret: 'whatever',
  resave: false,
  saveUninitialized: false,
}));
app.use(express.urlencoded({extended: false}));
const postgresDB = require('../../db/services/postgres-store');
const passport = require('../../loaders/passport.loader')(app);
require('../../routes/routes')(app, passport);


// eslint-disable-next-line require-jsdoc
async function initDB() {
  const config = {
    host: evt('POSTGRES_HOST'),
    port: evt('POSTGRES_PORT'),
    user: evt('POSTGRES_USER'),
    password: evt('POSTGRES_PASSWORD'),
    database: evt('POSTGRES_DB'),
  };
  await postgresDB.init(config);
  const alreadyExist = await postgresDB.client.query(`SELECT EXISTS (
    SELECT FROM information_schema.tables 
    WHERE  table_schema = 'wtg'
    AND    table_name   = 'SQUADS'
    ); `);
  if (!alreadyExist.rows[0].exists) {
    const dbContent = fs.readFileSync(path.join(__dirname, '../../db/scripts/init.sql'), {encoding: 'utf-8'});
    await postgresDB.client.query(dbContent);
  }
}

// eslint-disable-next-line require-jsdoc
async function cleanDB() {
  await postgresDB.client.query(`
  DELETE FROM "wtg"."SQUAD_ACCESS";
  DELETE FROM "wtg"."SQUADS";
  DELETE FROM "wtg"."WTG_PROFILES";
  DELETE FROM "wtg"."GIT_PROFILES";
  DELETE FROM "wtg"."USERS";
  `);
}

// eslint-disable-next-line require-jsdoc
async function createAgent() {
  const agent = request.agent(app);
  return agent;
}

// eslint-disable-next-line require-jsdoc
function closeDbConnection() {
  postgresDB.close();
}

// eslint-disable-next-line require-jsdoc
async function addNewUser(id) {
  const newUser = await GitProfiles.createGitProfile(id, 'gitlab');
  return newUser;
}

// eslint-disable-next-line require-jsdoc
async function check401NotAuthenticated(cb) {
  const agent = await createAgent();

  const response = await cb(agent);
  expect(response.statusCode).to.be.equal(401);

  expect(JSON.parse(response.text))
      .is.eql({
        error: 'Not authenticated',
      });
}

// eslint-disable-next-line require-jsdoc
async function check400BadRequest(cb) {
  const agent = await createAgent();
  await agent.get('/auth/gitlab')
      .withCredentials()
      .expect(302);

  const response = await cb(agent);
  expect(response.statusCode).to.equal(400);

  expect(JSON.parse(response.text))
      .is.eql({
        error: 'Bad request',
      });
}

// eslint-disable-next-line require-jsdoc
async function check404NotFound(cb) {
  const agent = await createAgent();
  await agent.get('/auth/gitlab')
      .withCredentials()
      .expect(302);

  const response = await cb(agent);
  expect(response.statusCode).to.equal(404);

  expect(JSON.parse(response.text))
      .is.eql({
        error: 'Not found',
      });
}

// eslint-disable-next-line require-jsdoc
async function shouldReturn404IfSquadNotExist(cb) {
  await check404NotFound(async (agent) => {
    const user = await agent.get('/auth/me')
        .expect(200);

    const squadArray = JSON.parse(user.text).squads;

    let maxSquadId = 0;

    for (const s of squadArray) {
      if (s.squad_id > maxSquadId) {
        maxSquadId = s.squad_id;
      }
    }
    return await cb(agent, maxSquadId + 1);
  });
}

// eslint-disable-next-line require-jsdoc
async function mockGitlabMembersAPI(projectId, projectMembers) {
  nock('https://gitlab.com')
      .get(`/api/v4/projects/${projectId}/members/all`)
      .reply(200,
          projectMembers,
      );
}

// eslint-disable-next-line require-jsdoc
async function callContributorsApiAfterMockingGitlab(projects, cb) {
  let returnObject = [];
  for (const project of projects) {
    if (typeof project !== 'object') {
      returnObject = project;
      break;
    }
    await mockGitlabMembersAPI(project.info.id, project.members);
    returnObject.push(project.info);
    nock('https://gitlab.com')
        .get(`/api/v4/projects/${project.id}/languages`)
        .reply(200,
            project.languages,
        );
    nock('https://gitlab.com')
        .get(`/api/v4/projects/${project.id}`)
        .reply(200,
            project.info,
        );
  }
  nock('https://gitlab.com')
      .get('/api/v4/projects?membership=true')
      .reply(200,
          returnObject,
      );
  const agent = await createAgent();
  await agent.get('/auth/gitlab')
      .withCredentials()
      .expect(302);
  const response = await agent.get(`/api/v1/gitlab/contributors`);
  await cb(agent, response);
}

// eslint-disable-next-line require-jsdoc
async function mockUserProfile(users, startDate, endDate) {
  for (const user of users) {
    nock('https://gitlab.com')
        .persist()
        .get('/api/v4/users/' + user.id)
        .reply(200,
            user.profile,
        );
    for (let i = 1; i < 3; i++) {
      let reply;
      if (i == 1) {
        reply = user.commits;
      } else {
        reply = [];
      }
      nock('https://gitlab.com')
          .persist()
          .filteringPath(/per_page=[^&]*/g, 'per_page=XXX')
          .get(`/api/v4/users/${user.id}/events?action=pushed&before=${startDate}&after=${endDate}&sort=desc&page=${i}&per_page=XXX`)
          .reply(200,
              reply,
          );
    }
  }
}

module.exports = {
  expect, initDB, createAgent, closeDbConnection, cleanDB, addNewUser, check401NotAuthenticated, check400BadRequest, check404NotFound, shouldReturn404IfSquadNotExist, callContributorsApiAfterMockingGitlab, mockUserProfile,
};
