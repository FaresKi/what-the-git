// https://github.com/marcosnils/passport-dev/blob/master/lib/strategy.js
const passport = require('passport-strategy');
const util = require('util');
const mockedUser = require('../mocked-objects/gitlab/mocked-profile');

/**
 * @param {String} name
 * @param {Function} _cb
 */
function MockStrategy(name, _cb) {
  if (!name || name.length === 0) {
    throw new TypeError('DevStrategy requires a Strategy name');
  }

  passport.Strategy.call(this);

  this.name = name;
  this.user = mockedUser;
  // Callback supplied to OAuth2 strategies handling verification
  this.cb = _cb;
}

util.inherits(MockStrategy, passport.Strategy);

// Need 2 different users
/**
 * @param {Object} req
 * @param {*} options
 */
MockStrategy.prototype.authenticate = function(req, options) {
  this.cb(process.env.TEST_TOKEN, null, this.user, (error, _user) => {
    this.success(_user);
  });
};

module.exports = MockStrategy;
