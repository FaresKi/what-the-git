const CryptoJS = require('crypto-js');
/**
 * Ciphering/Deciphering Helper
 */
class Crypto {
  /**
   * Ciphers a string
   * @param {string} str to cipher
   * @return {string} ciphered string
   */
  encrypt(str) {
    const ciphertext = CryptoJS.AES.encrypt(
        str,
        process.env.CRYPTO_KEY,
    ).toString();
    return ciphertext;
  }
  /**
   * Deciphers a string
   * @param {string} str to decipher
   * @return {string} deciphered string
   */
  decrypt(str) {
    const bytes = CryptoJS.AES.decrypt(str, process.env.CRYPTO_KEY);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  }
}

module.exports = new Crypto();
