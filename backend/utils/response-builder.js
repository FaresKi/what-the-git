const GitProfiles = require('../db/models/git-profiles');
const GitlabProfiles = require('../services/gitlab.profiles');
const GitlabProjects = require('../services/gitlab.projects');
const SquadAccess = require('../db/models/squad-access');
const Squads = require('../db/models/squads');
/**
 * @typedef GitlabProfile
 * @property {Number} profile_id profile ID on gitlab
 * @property {string} username
 * @property {string} name
 * @property {string} avatar_url
 * @property {string} web_url
 */

/**
 * @typedef Commit
 * @property {string} commit_id
 * @property {Number} project_id
 * @property {string} project_name
 * @property {string} project_url
 * @property {string} branch
 * @property {string} title
 * @property {string} committed_date
 */

/**
 * @typedef Activity
 * @property {Object} languages
 * @property {Commit.model} last_commit
 * @property {Array.<Commit>} commits
 */

/**
 * @typedef SquadMember
 * @property {Number} user_id
 * @property {GitlabProfile.model} gitlab
 * @property {Activity.model} activity
 */

/**
 * @typedef Squad
 * @property {Number} squad_id
 * @property {string} picture_url
 * @property {string} name
 * @property {SquadMember.model} owner
 * @property {Array.<SquadMember>} members
 */

/**
 * @typedef SquadAccess
 * @property {Number} user_id
 * @property {string} access_type
 */

/**
 * @typedef User
 * @property {Number} user_id - User's ID
 * @property {GitlabProfile.model} gitlab
 * @property {Array.<Squad>} squads - Accessible squads
 */

/**
 * Builds API responses
 */
class ResponseBuilder {
  /**
   * Builds a GitlabProfile from userId
   * @param {string} accessToken
   * @param {Number} userId
   * @return {Promise<GitlabProfile>}
   * or null If something went wrong
   */
  async buildGitlabProfileFromUserId(accessToken, userId) {
    try {
      const tmp = await GitProfiles.findGitProfileByUserId(userId, 'gitlab');
      const gitlabProfile = await GitlabProfiles.getProfile(
          tmp.ID,
          accessToken,
      );
      return {
        profile_id: gitlabProfile.id,
        name: gitlabProfile.name,
        username: gitlabProfile.username,
        avatar_url: gitlabProfile.avatar_url,
        web_url: gitlabProfile.web_url,
      };
    } catch (err) {
      console.log('err buildGitlabProfileFromUserId');
      return null;
    }
  }
  /**
   * Builds Gitlab Contributors from AccessToken
   * @param {string} accessToken
   * @return {Promise<Array<Object>>}
   */
  async buildGitlabContributorsFromAccessToken(accessToken) {
    const projects = await GitlabProjects.getProjects(accessToken);
    const contributors = [];
    const contributorsStr = new Set();
    try {
      for (const project of projects) {
        const members = await GitlabProjects.getProjectMembers(
            project.id,
            accessToken,
        );
        members.forEach((member) => {
          contributorsStr.add(
              JSON.stringify({
                profile_id: member.id,
                username: member.username,
                name: member.name,
                avatar_url: member.avatar_url,
                web_url: member.web_url,
              }),
          );
        });
      }

      for (const contributorStr of contributorsStr) {
        const parsedGitlab = JSON.parse(contributorStr);
        let gitlabProfile = await GitProfiles.findGitProfile(
            parsedGitlab.profile_id,
            'gitlab',
        );
        if (gitlabProfile == null) {
          gitlabProfile = await GitProfiles.createGitProfile(
              parsedGitlab.profile_id,
              'gitlab',
          );
        }
        contributors.push({
          user_id: gitlabProfile.USER_ID,
          gitlab: parsedGitlab,
          activity: null,
        });
      }
      return contributors;
    } catch (err) {
      console.log('err buildGitlabContributorsFromAccessToken ' + err);
      return null;
    }
  }
  /**
   * Builds activity from profileId
   * @param {string} accessToken
   * @param {string} startDate
   * @param {string} endDate
   * @param {string} profileId
   * @param {Boolean} onlyLastActivity
   * @return {Promise<Activity>}
   */
  async buildActivityFromProfileId(
      accessToken,
      startDate,
      endDate,
      profileId,
      onlyLastActivity = true,
  ) {
    try {
      const activity = {};
      const languages = await GitlabProfiles.getProjectsLanguages(
          profileId,
          startDate,
          endDate,
          accessToken,
      );
      activity.languages = languages;
      const tmp = await GitlabProfiles.getCommitActivity(
          profileId,
          startDate,
          endDate,
          onlyLastActivity,
          accessToken,
      );
      const projects = {};
      const commits = [];
      for (const elt of tmp) {
        const projectId = elt.project_id.toString();
        if (!projects.hasOwnProperty(projectId)) {
          projects[projectId] = await GitlabProjects.getProject(
              elt.project_id,
              accessToken,
          );
        }
        commits.push({
          commit_id: elt.push_data.commit_to,
          project_id: elt.project_id,
          project_name: projects[projectId].name,
          project_url: projects[projectId].web_url,
          branch: elt.push_data.ref,
          title: elt.push_data.commit_title,
          committed_date: elt.created_at,
        });
      }
      activity.last_commit = commits[0] ?? {};
      activity.commits = commits;
      if (commits.length == 0) {
        return null;
      }
      return activity;
    } catch (err) {
      console.log('buildActivityFromProfileId ' + err);
      return null;
    }
  }

  /**
   * Builds a SquadMember from userId
   * @param {string} accessToken
   * @param {Number} userId
   * @param {string} startDate
   * @param {string} endDate
   * @return {Promise<SquadMember>}
   * or null If something went wrong
   */
  async buildSquadMemberFromUserId(
      accessToken,
      userId,
      startDate = null,
      endDate = null,
  ) {
    try {
      const tmp = await GitProfiles.findGitProfileByUserId(userId, 'gitlab');
      const gitlabProfile = await this.buildGitlabProfileFromUserId(
          accessToken,
          tmp.USER_ID,
      );
      let activity = null;
      if (startDate != null && endDate != null) {
        activity = await this.buildActivityFromProfileId(
            accessToken,
            startDate,
            endDate,
            // @ts-ignore
            gitlabProfile.profile_id,
        );
      }
      return {
        user_id: userId,
        gitlab: gitlabProfile,
        activity: activity,
      };
    } catch (err) {
      console.log('buildSquadMemberFromUserId ' + err);
      return null;
    }
  }

  /**
   * Build SquadAccess from squadId
   * @param {Number} squadId
   * @return {Promise<SquadAccess>}
   * or null if something goes wrong
   */
  async buildSquadAccessFromSquadId(squadId) {
    try {
      const squad = await Squads.findSquad(squadId);
      if (squad == null) {
        return null;
      }
      const squadAccess = await SquadAccess.findSquadAccess(squadId);
      if (squadAccess == null) {
        return null;
      }
      const accessList = [];
      for (const access of squadAccess) {
        accessList.push({
          // @ts-ignore
          user_id: access.USER_ID,
          // @ts-ignore
          access_type: access.ACCESS_TYPE,
        });
      }
      // @ts-ignore
      return accessList;
    } catch (err) {
      console.log('buildSquadAccessFromSquadId ' + err);
      return null;
    }
  }

  /**
   * Builds a Squad from squadId
   * @param {string} accessToken
   * @param {Number} squadId
   * @param {string} startDate
   * @param {string} endDate
   * @return {Promise<Squad>}
   * or null If something went wrong
   */
  async buildSquadFromSquadId(
      accessToken,
      squadId,
      startDate = null,
      endDate = null,
  ) {
    try {
      const tmp = await Squads.findSquad(squadId);
      const members = [];
      const owner = await this.buildSquadMemberFromUserId(
          accessToken,
          tmp.OWNER_ID,
      );
      if (tmp.MEMBERS_IDS !== null) {
        for (const id of tmp.MEMBERS_IDS) {
          const member = await this.buildSquadMemberFromUserId(
              accessToken,
              id,
              startDate,
              endDate,
          );
          if (member !== null) {
            members.push(member);
          }
        }
      }
      return {
        squad_id: tmp.ID,
        picture_url: tmp.PICTURE_URL,
        name: tmp.DISPLAY_NAME,
        owner: owner,
        members: members,
      };
    } catch (err) {
      console.log('buildSquadFromSquadId ' + err);
      return null;
    }
  }
  /**
   * Builds a Squad from userId & displayName
   * @param {string} accessToken
   * @param {Number} ownerId
   * @param {string} displayName
   * @param {string} startDate
   * @param {string} endDate
   * @return {Promise<Squad>}
   * or null If something went wrong
   */
  async buildSquadFromSquadDisplayName(
      accessToken,
      ownerId,
      displayName,
      startDate,
      endDate,
  ) {
    try {
      const tmp = await Squads.findSquadByName(ownerId, displayName);
      const members = [];
      const owner = await this.buildSquadMemberFromUserId(
          accessToken,
          tmp.OWNER_ID,
      );
      if (tmp.MEMBERS_IDS !== null) {
        for (const id of tmp.MEMBERS_IDS) {
          const member = await this.buildSquadMemberFromUserId(
              accessToken,
              id,
              startDate,
              endDate,
          );
          if (member !== null) {
            members.push(member);
          }
        }
      }
      return {
        squad_id: tmp.ID,
        picture_url: tmp.PICTURE_URL,
        name: tmp.DISPLAY_NAME,
        owner: owner,
        members: members,
      };
    } catch (err) {
      console.log('buildSquadFromSquadId ' + err);
      return null;
    }
  }
  /**
   * Builds a User from userId
   * @param {string} accessToken
   * @param {Number} userId
   * @return {Promise<User>}
   * or null If something went wrong
   */
  async buildUserFromUserId(accessToken, userId) {
    try {
      const tmp = await Squads.findSquadsByUserAccessType(userId, 'R');
      const gitlab = await this.buildGitlabProfileFromUserId(
          accessToken,
          userId,
      );
      const squads = [];
      if (tmp !== null) {
        for (const squad of tmp) {
          const builtSquad = await this.buildSquadFromSquadId(
              accessToken,
              squad.ID,
          );
          if (builtSquad !== null) {
            squads.push(builtSquad);
          }
        }
        return {
          user_id: userId,
          gitlab: gitlab,
          squads: squads,
        };
      }
    } catch (err) {
      console.log('buildUserFromUserId ' + err);
      return null;
    }
  }
}

module.exports = new ResponseBuilder();
