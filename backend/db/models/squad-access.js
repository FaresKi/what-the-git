const PostgresStore = require('../services/postgres-store');
const sql = require('pg-sql2');

const schemaName = 'wtg';
const tableName = 'SQUAD_ACCESS';

/**
 * C.R.U.D. Interactions with wtg.SQUAD_ACCESS
 */
class SquadAccess {
  /**
   * @typedef {Object} SquadAccess
   * @property {Number} SQUAD_ID Squad's ID
   * @property {Number} USER_ID User's ID
   * @property {string} ACCESS_TYPE Read / ReadWrite permissions on Squad
   * @property {string} UPDATE_TIMESTAMP
   * Timestamp representing last Squad modification
   */

  /**
   * Creates a new record in wtg.SQUAD_ACCESS
   * @param {Number} squadId Squad's ID
   * @param {Number} userId User's ID
   * @param {string} accessType Read / ReadWrite permissions on Squad
   * @return {Promise<SquadAccess>} Returning created squad access
   * or null if failed
   */
  async createSquadAccess(squadId, userId, accessType) {
    try {
      const fields = sql.join(
          [
            sql.identifier('SQUAD_ID'),
            sql.identifier('USER_ID'),
            sql.identifier('ACCESS_TYPE'),
          ],
          ',',
      );
      const query = sql.query`INSERT INTO 
      ${sql.identifier(schemaName, tableName)}(${fields})
      VALUES(
        ${sql.value(squadId)},
        ${sql.value(userId)},
        ${sql.value(accessType)}) RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount !== 1) {
        return null;
      }
      const squadAccess = rs.rows[0];
      return squadAccess;
    } catch (err) {
      console.log('createSquadAccess ' + err);
      return null;
    }
  }

  /**
   * Search for a record in wtg.SQUAD_ACCESS
   * @param {Number} squadId Squad's ID
   * @param {Number} userId User's ID
   * @return {Promise<SquadAccess>} Found SquadAccess
   * or null If no corresponding SquadAcess found
   */
  async findSquadAccessByUserId(squadId, userId) {
    try {
      const query = sql.query`SELECT * FROM
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('USER_ID')} =  ${sql.value(userId)}
      AND  ${sql.identifier('SQUAD_ID')} =  ${sql.value(squadId)}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount !== 1) {
        return null;
      }
      const squadAccess = rs.rows[0];
      return squadAccess;
    } catch (err) {
      console.log(`findSquadAccess: ${err}`);
      return null;
    }
  }
  /**
   * Search for records in wtg.SQUAD_ACCESS
   * @param {Number} squadId Squad's ID
   * @return {Promise<Array.<SquadAccess>>} Found SquadAccess
   * or null If no corresponding SquadAcess found
   */
  async findSquadAccess(squadId) {
    try {
      const query = sql.query`SELECT * FROM
      ${sql.identifier(schemaName, tableName)}
      WHERE  ${sql.identifier('SQUAD_ID')} =  ${sql.value(squadId)}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount == 0) {
        return null;
      }
      return rs.rows;
    } catch (err) {
      console.log(`findSquadAccess: ${err}`);
      return null;
    }
  }
  /**
   * Updates a record in wtg.SQUAD_ACCESS
   * @param {Number} squadId Squad's ID
   * @param {Number} userId User's ID
   * @param {string} accessType Read / ReadWrite permissions on Squad
   * @return {Promise<SquadAccess>} Updated SquadAccess
   * or null If something went wrong
   */
  async updateSquadAccess(squadId, userId, accessType) {
    try {
      const query = sql.query`UPDATE ${sql.identifier(schemaName, tableName)}
      SET ${sql.identifier('ACCESS_TYPE')} =  ${sql.value(accessType)},
        "UPDATE_TIMESTAMP" = current_timestamp
      WHERE ${sql.identifier('SQUAD_ID')} =  ${sql.value(squadId)} AND
      ${sql.identifier('USER_ID')} =  ${sql.value(userId)} RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const squadAccess = rs.rows[0];
      return squadAccess;
    } catch (err) {
      console.log(`updateSquadAccess: ${err}`);
      return null;
    }
  }
  /**
   * Deletes a record in wtg.SQUAD_ACCESS
   * @param {Number} squadId Squad's ID
   * @param {Number} userId User's ID
   * @return {Promise<SquadAccess>} Deleted SquadAccess
   * or null If something went wrong
   */
  async deleteSquadAccess(squadId, userId) {
    try {
      const query = sql.query`DELETE FROM ${sql.identifier(
          schemaName,
          tableName,
      )}
      WHERE ${sql.identifier('SQUAD_ID')} =  ${sql.value(squadId)} AND
      ${sql.identifier('USER_ID')} =  ${sql.value(userId)} RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount < 1) {
        return null;
      }
      const squadAccess = rs.rows[0];
      return squadAccess;
    } catch (err) {
      console.log(`deleteSquadAccess: ${err}`);
      return null;
    }
  }
}

module.exports = new SquadAccess();
