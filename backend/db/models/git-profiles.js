const PostgresStore = require('../services/postgres-store');
const Users = require('./users');
const sql = require('pg-sql2');
const schemaName = 'wtg';
const tableName = 'GIT_PROFILES';

/**
 * C.R.U.D. Interactions with wtg.GIT_PROFILES
 */
class GitProfiles {
  /**
   * @typedef {Object} GitProfile Object representing a wtg.GIT_PROFILES record
   * @property {Number} ID ID of the user on the provider website
   * @property {string} PROVIDER Git provider
   * @property {string} ACCESS_TOKEN
   * Access token used to perform queries on provider's API (encrypted)
   * @property {string} REFRESH_TOKEN
   * Refresh token used to renew access token (encrypted)
   * @property {string} UPDATE_TIMESTAMP
   * Timestamp of the last modification on the record
   * @property {Number} USER_ID
   * ID of the user associated with the current GIT_PROFILES record
   */

  /**
   * Creates a new record in wtg.GIT_PROFILES
   * @param {Number} profileId ID of the user on the provider website
   * @param {string} provider Git provider
   * @param {string} accessToken
   * Access token used to perform queries on provider's API (encrypted)
   * @param {string} refreshToken
   * Refresh token used to renew access token (encrypted)
   * @param {Number} userId
   * ID of the user associated with the current GIT_PROFILES record
   * @return {Promise<GitProfile>} Created GitProfile
   */
  async createGitProfile(
      profileId,
      provider,
      accessToken = null,
      refreshToken = null,
      userId = null,
  ) {
    try {
      let user = null;
      if (userId == null) {
        user = await Users.createUser();
      } else {
        user = await Users.findUser(userId);
      }
      const fields = sql.join(
          [
            sql.identifier('ID'),
            sql.identifier('PROVIDER'),
            sql.identifier('USER_ID'),
            sql.identifier('REFRESH_TOKEN'),
            sql.identifier('ACCESS_TOKEN'),
          ],
          ',',
      );
      const query = sql.query`INSERT INTO 
    ${sql.identifier(schemaName, tableName)}(${fields})
    VALUES (${sql.value(profileId)}, 
    ${sql.value(provider)}, 
      ${sql.value(user.ID)},
      ${sql.value(refreshToken)},
      ${sql.value(accessToken)})  RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      const gitProfile = rs.rows[0];
      return gitProfile;
    } catch (err) {
      console.log('createGitProfile ' + err);
      return null;
    }
  }
  /**
   * Searches for a record in wtg.GIT_PROFILES
   * @param {Number} profileId ID of the user on the provider website
   * @param {string} provider Git provider
   * @return {Promise<GitProfile> | null} Found GitProfile or
   * Null if no corresponding GitProfile found
   */
  async findGitProfile(profileId, provider) {
    try {
      const query = sql.query`SELECT * FROM ${sql.identifier(
          schemaName,
          tableName,
      )} 
      WHERE ${sql.identifier('ID')} = ${sql.value(profileId)} 
      AND ${sql.identifier('PROVIDER')} = ${sql.value(provider)}`;
      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount < 1) {
        return null;
      }

      const gitProfile = rs.rows[0];
      return gitProfile;
    } catch (err) {
      console.log(`findGitProfileErr: ${err}`);
      return null;
    }
  }
  /**
   * Searches for a record in wtg.GIT_PROFILES by its USER_ID
   * @param {Number} userId User's ID
   * @param {string} provider Git provider
   * @return {Promise<GitProfile> | null} Found GitProfile
   * If no corresponding GitProfile found
   */
  async findGitProfileByUserId(userId, provider) {
    try {
      const query = sql.query`SELECT * FROM ${sql.identifier(
          schemaName,
          tableName,
      )} 
      WHERE ${sql.identifier('USER_ID')} = ${sql.value(userId)} 
      AND ${sql.identifier('PROVIDER')} = ${sql.value(provider)}`;
      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount < 1) {
        return null;
      }

      const gitProfile = rs.rows[0];
      return gitProfile;
    } catch (err) {
      console.log(`findGitProfileErr: ${err}`);
      return null;
    }
  }
  /**
   * Updates a record in wtg.GIT_PROFILES
   * @param {Number} profileId ID of the user on the provider website
   * @param {string} provider Git provider
   * @param {string} accessToken
   * Access token used to perform queries on provider's API (encrypted)
   * @param {string} refreshToken
   * Refresh token used to renew access token (encrypted)
   * @param {Number} userId
   * ID of the user associated with the current GIT_PROFILES record
   * @return {Promise<GitProfile>} Updated GitProfile
   * or null If something went wrong
   */
  async updateGitProfile(
      profileId,
      provider,
      accessToken,
      refreshToken,
      userId,
  ) {
    try {
      const query = sql.query`UPDATE ${sql.identifier(schemaName, tableName)} 
      SET "UPDATE_TIMESTAMP" = current_timestamp,
      ${sql.identifier('ACCESS_TOKEN')} = ${sql.value(accessToken)},
      ${sql.identifier('REFRESH_TOKEN')} = ${sql.value(refreshToken)},
      ${sql.identifier('USER_ID')} = ${sql.value(userId)} WHERE
      ${sql.identifier('PROVIDER')} = ${sql.value(provider)} AND
      ${sql.identifier('ID')} = ${sql.value(profileId)} RETURNING *`;
      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const gitProfile = rs.rows[0];
      return gitProfile;
    } catch (err) {
      console.log(`updateGitProfile: ${err}`);
      return null;
    }
  }
  /**
   * Deletes a record in wtg.GIT_PROFILES
   * @param {Number} profileId ID of the user on the provider website
   * @param {string} provider Git provider
   * @return {Promise<GitProfile>} Deleted GitProfile
   * or null If something went wrong
   */
  async deleteGitProfile(profileId, provider) {
    try {
      const query = sql.query`DELETE FROM ${sql.identifier(
          schemaName,
          tableName,
      )} 
      WHERE ${sql.identifier('PROVIDER')} = ${sql.value(provider)}
      AND ${sql.identifier('ID')} = ${sql.value(profileId)} RETURNING *`;
      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const gitProfile = rs.rows[0];
      return gitProfile;
    } catch (err) {
      console.log(`deleteGitProfile: ${err}`);
      return null;
    }
  }
}

module.exports = new GitProfiles();
