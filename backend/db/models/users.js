const PostgresStore = require('../services/postgres-store');
const sql = require('pg-sql2');
const schemaName = 'wtg';
const tableName = 'USERS';

/**
 * C.R.D. Interactions with wtg.USERS
 */
class Users {
  /**
   * @typedef {Object} User Object representing a wtg.USERS record
   * @property {Number} ID User's ID
   * @property {string} CREATION_TIMESTAMP
   * Timestamp of the record creation
   */

  /**
   * Creates a new record in wtg.USERS
   * @return {Promise<User>} Created User
   */
  async createUser() {
    const rs = await PostgresStore.client
        .query(`INSERT INTO "wtg"."USERS"("CREATION_TIMESTAMP") 
        VALUES (current_timestamp) RETURNING *`);
    const user = rs.rows[0];
    return user;
  }
  /**
   * Searches for a record in wtg.USERS
   * @param {Number} userId User's ID
   * @return {Promise<User>} Found User
   * or null If no corresponding User was found
   */
  async findUser(userId) {
    try {
      const query = sql.query`SELECT * FROM 
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('ID')} =  ${sql.value(userId)}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const user = rs.rows[0];
      return user;
    } catch (err) {
      console.log(`findUser: ${err}`);
      return null;
    }
  }
  /**
   * Deletes a record in wtg.USERS
   * @param {Number} userId User's ID
   * @return {Promise<User>} Deleted User
   * or null If something went wrong
   */
  async deleteUser(userId) {
    try {
      const query = sql.query`DELETE FROM 
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('ID')} =  ${sql.value(userId)}`;
      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const user = rs.rows[0];
      return user;
    } catch (err) {
      console.log(`deleteUser: ${err}`);
      return null;
    }
  }
}

module.exports = new Users();
