const sql = require('pg-sql2');

const schemaName = 'wtg';
const tableName = 'SQUADS';

const PostgresStore = require('../services/postgres-store');
const SquadAccess = require('./squad-access');
/**
 * C.R.U.D. Interactions with wtg.SQUADS
 */
class Squads {
  /**
   * @typedef {Object} Squad Object representing a wtg.SQUADS record
   * @property {Number} ID Squad's ID
   * @property {string} PICTURE_URL URL of Squad's profile picture
   * @property {string} CREATION_TIMESTAMP
   * Timestamp representing Squad's creation
   * @property {string} DISPLAY_NAME Squad's name
   * @property {string} UPDATE_TIMESTAMP
   * Timestamp representing last Squad modification
   * @property {Array.<Number>} MEMBERS_IDS
   * User's ids of Squad members
   * @property {Number} OWNER_ID User's id of Squad owner
   */

  /**
   * Creates a new record in wtg.SQUADS
   * @param {Number} ownerId User's id of Squad owner
   * @param {string} displayName Squad's name
   * @return {Promise<Squad>} Created Squad
   */
  async createSquad(ownerId, displayName) {
    try {
      const fields = sql.join(
          [
            sql.identifier('DISPLAY_NAME'),
            sql.identifier('OWNER_ID'),
            sql.identifier('MEMBERS_IDS'),
          ],
          ',',
      );
      const query = sql.query`INSERT INTO 
      ${sql.identifier(schemaName, tableName)}(${fields})
      VALUES(
        ${sql.value(displayName)},
        ${sql.value(ownerId)},
        ${sql.value([ownerId])}) RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount !== 1) {
        return null;
      }
      const squad = rs.rows[0];
      const rs2 = await SquadAccess.createSquadAccess(squad.ID, ownerId, 'RW');
      if (rs2 === null) {
        return null;
      }
      return squad;
    } catch (err) {
      console.log(`createSquad: ${err}`);
      return null;
    }
  }

  /**
   * Searches for a record in wtg.SQUADS
   * @param {Number} squadId Squad's ID
   * @return {Promise<Squad>} Found Squad
   * or null If no corresponding Squad found
   */
  async findSquad(squadId) {
    try {
      const query = sql.query`SELECT * FROM
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('ID')} = ${sql.value(squadId)}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);

      if (rs.rowCount < 1) {
        return null;
      }
      const squad = rs.rows[0];
      return squad;
    } catch (err) {
      console.log(`findSquad: ${err}`);
      return null;
    }
  }

  /**
   * Searches for a record in wtg.SQUADS by its DISPLAY_NAME
   * @param {Number} ownerId
   * @param {String} displayName
   * @return {Promise<Squad>} Found Squad
   * or null If no corresponding squad was found
   */
  async findSquadByName(ownerId, displayName) {
    try {
      const query = sql.query`SELECT * FROM
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('OWNER_ID')} = ${sql.value(ownerId)}
      AND ${sql.identifier('DISPLAY_NAME')} = ${sql.value(displayName)}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount !== 1) {
        return null;
      }
      return rs.rows[0];
    } catch (err) {
      console.log(`findSquadByName: ${err}`);
      return null;
    }
  }
  /**
   * Searches for records in wtg.SQUADS by its User's access type
   * @param {Number} userId User's ID
   * @param {string} accessType Read / ReadWrite permissions on Squad
   * @return {Promise<Array.<Squad>>}
   * Squads on which user has requested ACCESS_TYPE
   * or null If no squads were found
   */
  async findSquadsByUserAccessType(userId, accessType) {
    try {
      const query = sql.query`SELECT DISTINCT A.* FROM
      ${sql.identifier(schemaName, tableName)} A
      INNER JOIN ${sql.identifier(schemaName, 'SQUAD_ACCESS')} B
      ON A.${sql.identifier('OWNER_ID')} = ${sql.value(userId)}
      AND B.${sql.identifier('ACCESS_TYPE')} 
      LIKE ${sql.value('%' + accessType + '%')}`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      return rs.rows;
    } catch (err) {
      console.log(`findSquadsByUserAccessType: ${err}`);
      return null;
    }
  }
  /**
   * Updates a record in wtg.SQUADS
   * @param {Number} squadId
   * @param {string} pictureUrl
   * @param {string} displayName
   * @param {Array.<Number>} membersIDs
   * @param {Number} ownerId
   * @return {Promise<Squad>} Updated Squad
   * or null If something went wrong
   */
  async updateSquad(squadId, pictureUrl, displayName, membersIDs, ownerId) {
    try {
      const query = sql.query`UPDATE 
      ${sql.identifier(schemaName, tableName)}
      SET "UPDATE_TIMESTAMP" = current_timestamp,
      ${sql.identifier('PICTURE_URL')} = ${sql.value(pictureUrl)},
      ${sql.identifier('DISPLAY_NAME')} = ${sql.value(displayName)},
      ${sql.identifier('MEMBERS_IDS')} = ${sql.value(membersIDs)},
      ${sql.identifier('OWNER_ID')} = ${sql.value(ownerId)}
      WHERE ${sql.identifier('ID')} =  ${sql.value(squadId)}
      RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const squad = rs.rows[0];
      return squad;
    } catch (err) {
      console.log(`updateSquad: ${err}`);
      return null;
    }
  }
  /**
   * Deletes a record in wtg.GIT_PROFILES
   * @param {Number} squadId Squad's ID
   * @return {Promise<Squad>} Deleted Squad
   * or null If something went wrong
   */
  async deleteSquad(squadId) {
    try {
      const query = sql.query`DELETE FROM 
      ${sql.identifier(schemaName, tableName)}
      WHERE ${sql.identifier('ID')} =  ${sql.value(squadId)}
      RETURNING *`;

      const {text, values} = sql.compile(query);
      const rs = await PostgresStore.client.query(text, values);
      if (rs.rowCount < 1) {
        return null;
      }
      const squad = rs.rows[0];
      return squad;
    } catch (err) {
      console.log(`deleteSquad: ${err}`);
      return null;
    }
  }
}

module.exports = new Squads();
