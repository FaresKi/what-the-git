CREATE SCHEMA "wtg";

CREATE TABLE "wtg"."USERS"
(
 "ID"                 serial NOT NULL,
 "CREATION_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 CONSTRAINT "PK_users" PRIMARY KEY ( "ID" )
);

CREATE TABLE "wtg"."WTG_PROFILES"
(
 "ID"                 serial NOT NULL,
 "DISPLAY_NAME"       text NOT NULL,
 "PICTURE_URL"        text NULL,
 "USERNAME"           text NOT NULL,
 "PASSWORD"           text NOT NULL,
 "REGISTER_TIMESTAMP" timestamp NOT NULL,
 "UPDATE_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 "USER_ID"            integer NOT NULL,
 CONSTRAINT "PK_wtg_profiles" PRIMARY KEY ( "ID" ),
 CONSTRAINT "USER_WTG_FK" FOREIGN KEY ( "USER_ID" ) REFERENCES "wtg"."USERS" ( "ID" ) ON DELETE CASCADE
);

CREATE INDEX "fkIdx_25" ON "wtg"."WTG_PROFILES"
(
 "USER_ID"
);

CREATE TABLE "wtg"."GIT_PROFILES"
(
 "ID"            serial NOT NULL,
 "PROVIDER"      text NOT NULL,
 "ACCESS_TOKEN"  text NULL,
 "REFRESH_TOKEN" text NULL,
 "UPDATE_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 "USER_ID"       integer NOT NULL,
 CONSTRAINT "PK_git_profiles" PRIMARY KEY ( "ID", "PROVIDER" ),
 CONSTRAINT "USER_GIT_FK" FOREIGN KEY ( "USER_ID" ) REFERENCES "wtg"."USERS" ( "ID" ) ON DELETE CASCADE
);

CREATE INDEX "fkIdx_16" ON "wtg"."GIT_PROFILES"
(
 "USER_ID"
);


CREATE TABLE "wtg"."SQUADS"
(
 "ID"                 serial NOT NULL,
 "PICTURE_URL"        text NULL,
 "CREATION_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 "DISPLAY_NAME"       text NOT NULL,
 "UPDATE_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 "MEMBERS_IDS"        integer[] NOT NULL,
 "OWNER_ID"           integer NOT NULL,
 CONSTRAINT "PK_squads" PRIMARY KEY ( "ID" ),
 CONSTRAINT "FK_98" FOREIGN KEY ( "OWNER_ID" ) REFERENCES "wtg"."USERS" ( "ID" ) ON DELETE CASCADE
);

CREATE INDEX "fkIdx_98" ON "wtg"."SQUADS"
(
 "OWNER_ID"
);

CREATE TABLE "wtg"."SQUAD_ACCESS"
(
 "SQUAD_ID"    integer NOT NULL,
 "USER_ID"     integer NOT NULL,
 "ACCESS_TYPE" text NOT NULL,
 "CREATION_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 "UPDATE_TIMESTAMP" timestamp NOT NULL DEFAULT current_timestamp,
 CONSTRAINT "PK_squad_access" PRIMARY KEY ( "SQUAD_ID", "USER_ID" ),
 CONSTRAINT "FK_107" FOREIGN KEY ( "SQUAD_ID" ) REFERENCES "wtg"."SQUADS" ( "ID" ) ON DELETE CASCADE,
 CONSTRAINT "FK_79" FOREIGN KEY ( "USER_ID" ) REFERENCES "wtg"."USERS" ( "ID" ) ON DELETE CASCADE
);

CREATE INDEX "fkIdx_107" ON "wtg"."SQUAD_ACCESS"
(
 "SQUAD_ID"
);

CREATE INDEX "fkIdx_79" ON "wtg"."SQUAD_ACCESS"
(
 "USER_ID"
);

