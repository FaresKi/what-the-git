/* eslint-disable prefer-const */
/* eslint-disable require-jsdoc */
const moment = require('moment');
const GitlabProjects = require('./gitlab.projects');
const axios = require('axios');
class GitlabProfiles {
  async getCommitActivity(
      profileId,
      startDate,
      endDate,
      onlyLast,
      accessToken,
  ) {
    let after = moment(startDate);
    let before = moment(endDate);

    after.subtract(1, 'days');
    before.add(1, 'days');

    let page = 1;
    let perPage = 100;
    if (onlyLast) {
      perPage = 1;
    }
    let config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/users/${profileId}/events`,
      params: {
        action: 'pushed',
        before: before.format('YYYY-MM-DD'),
        after: after.format('YYYY-MM-DD'),
        sort: 'desc',
        page: page,
        per_page: perPage,
      },
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    let data = [];

    while (true) {
      let response = await axios(config);
      if (response.data.length == 0) {
        return data;
      } else {
        config.params.page = ++page;
        data = data.concat(response.data);
        if (onlyLast) {
          return data;
        }
      }
    }
  }

  async getProjectsActivityIds(profileId, startDate, endDate, accessToken) {
    const commits = await this.getCommitActivity(
        profileId,
        startDate,
        endDate,
        false,
        accessToken,
    );
    const projects = new Set();
    for (let commit of commits) {
      let projectId = commit.project_id;
      projects.add(projectId);
    }
    return Array.from(projects);
  }
  async getProjectsLanguages(profileId, startDate, endDate, accessToken) {
    const projectsIds = await this.getProjectsActivityIds(
        profileId,
        startDate,
        endDate,
        accessToken,
    );
    let userLanguages = {};
    let projectNB = 0;
    for (let projectId of projectsIds) {
      const languages = await GitlabProjects.getProjectLanguages(
          projectId,
          accessToken,
      );
      if (Object.keys(languages).length == 0) {
        continue;
      }
      projectNB++;
      Object.keys(languages).forEach((key) => {
        userLanguages[key] = (userLanguages[key] || 0) + languages[key];
      });
    }
    Object.keys(userLanguages).forEach((key) => {
      userLanguages[key] /= projectNB;
    });
    return userLanguages;
  }
  async getProfile(profileId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/users/${profileId}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }
}
module.exports = new GitlabProfiles();
