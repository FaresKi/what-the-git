const axios = require('axios');

/**
 * Handles Gitlab API interactions
 */
class Gitlab {
  /**
   * Return /api/v4/projects/:projectId/repository/commits for each branches
   * @param {Number} projectId
   * @param {string} accessToken
   * @param {string} _since ISO 8601 format YYYY-MM-DDTHH:MM:SSZ
   * @param {string} _until ISO 8601 format YYYY-MM-DDTHH:MM:SSZ
   * @return {Array} The projects' members' commits
   */
  async getProjectCommits(projectId, accessToken, _since, _until) {
    const branches = await this.getProjectBranches(projectId, accessToken);
    const commits = [];
    for (const branch of branches) {
      const config = {
        method: 'get',
        url: `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
        params: {
          with_stats: true,
          since: _since,
          until: _until,
          ref_name: branch.name,
        },
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      };
      const response = await axios(config);
      const branchCommits = [];
      response.data.forEach((elt) => {
        branchCommits.push({
          id: elt.id,
          title: elt.title,
          author_name: elt.author_name,
          created_at: elt.created_at,
        });
      });
      commits.push({
        name: branch.name,
        commits: branchCommits,
      });
    }
    return commits;
  }

  /**
   * Return api/v4/projects/:id/languages
   * @param {Number} projectId
   * @param {string} accessToken
   * @return {JSON} Languages
   */
  async getProjectLanguages(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/languages`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }

  /**
   * s
   * @param {*} accessToken
   */
  async getCollaborators(accessToken) {
    const projects = await this.getProjects(accessToken);
    const set = new Set();
    for (const project of projects) {
      // eslint-disable-next-line max-len
      const projectMembers = await this.getProjectMembers(
          project.id,
          accessToken,
      );
      projectMembers.forEach((member) => {
        const memberInfo = JSON.stringify({
          id: member.id,
          name: member.name,
          username: member.username,
          avatar_url: member.avatar_url,
        });
        // checks for duplicates, avoids wasting time processing
        set.add(memberInfo);
      });
    }
    // convert JSON string into JSON Object and push into Array.
    const collaborators = [];
    set.forEach((collaborator) => {
      collaborators.push(JSON.parse(collaborator));
    });

    return collaborators;
  }
  /**
   * Returns /api/v4/user from gitlab
   * @param {string} accessToken
   * @return {Object} The profile of the current user (accessToken owner)
   */
  async getUser(accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/user`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return {
      id: response.data.id,
      username: response.data.username,
      name: response.data.name,
      avatar_url: response.data.avatar_url,
      web_url: response.data.web_url,
    };
  }

  /**
   * Returns /api/v4/users/:profileId from gitlab
   * @param {string} accessToken
   * @param {Number} profileId
   * @return {Object} The profile of the user
   */
  async getProfile(accessToken, profileId) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/users/${profileId}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return {
      id: response.data.id,
      username: response.data.username,
      name: response.data.name,
      avatar_url: response.data.avatar_url,
      web_url: response.data.web_url,
    };
  }
  /**
   * Return /api/v4/projects from gitlab
   * @param {string} accessToken
   * @return {JSON} The projects
   * in which the current user (accessToken owner) participates
   */
  async getProjects(accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects?membership=true`,
      params: {
        membership: true,
      },
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    const projects = [];

    response.data.forEach((elt) => {
      projects.push({
        id: elt.id,
        description: elt.description,
        web_url: elt.web_url,
        name: elt.name,
      });
    });
    return projects;
  }

  /**
   * Return /api/v4/projects/:projectId/members from gitlab
   * @param {Number} projectId
   * @param {string} accessToken
   * @return {JSON} The projects' members
   * in which the current user (accessToken owner) participates
   */
  async getProjectMembers(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/members`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }
  /**
   * s
   * @param {*} projectId
   * @param {*} accessToken
   */
  async getProjectBranches(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/repository/branches`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    const branches = [];

    response.data.forEach((elt) => {
      branches.push({
        name: elt.name,
      });
    });
    return branches;
  }

  /**
   * Return /api/v4/projects/:projectId
   * /events?target_type=merge_request from gitlab
   * @param {Number} projectId
   * @param {String} accessToken
   * @return {Object} The projects' members' merge requests
   * in which the current user (accessToken owner) participates
   */
  async getProjectMergeRequests(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/events?target_type=merge_request`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }

  /**
   * Return /api/v4/projects/:projectId/issues from gitlab
   * @param {Number} projectId
   * @param {string} accessToken
   * @return {Object} The projects' members' issues
   * in which the current user (accessToken owner) participates
   */
  async getProjectIssues(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/issues`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }
}

module.exports = new Gitlab();
