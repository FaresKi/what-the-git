/* eslint-disable require-jsdoc */
const axios = require('axios');

/**
 * Handles Gitlab API interactions
 */
class GitlabProjects {
  async getProjects(accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects`,
      params: {
        membership: true,
      },
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }
  async getProject(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }

  async getProjectLanguages(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/languages`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }

  async getProjectMembers(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/members/all`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }

  async getProjectBranches(projectId, accessToken) {
    const config = {
      method: 'get',
      url: `https://gitlab.com/api/v4/projects/${projectId}/repository/branches`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    const response = await axios(config);
    return response.data;
  }
  async getProjectDetails(projectId, accessToken) {
    const project = await this.getProject(projectId, accessToken);
    const members = await this.getProjectMembers(projectId, accessToken);
    const languages = await this.getProjectLanguages(projectId, accessToken);
    const branches = await this.getProjectBranches(projectId, accessToken);

    const membersIds = [];
    members.forEach((member) => {
      membersIds.push(member.id);
    });

    const branchesLw = [];
    branches.forEach((branch) => {
      branchesLw.push({
        name: branch.name,
        commit: {
          commit_id: branch.commit.id,
          project_id: projectId,
          branch: branch.name,
          title: branch.commit.title,
          committed_date: branch.commit.committed_date,
        },
      });
    });
    return {
      project_id: project.id,
      name: project.name,
      web_url: project.web_url,
      languages: languages,
      membersIds: membersIds,
      branches: branchesLw,
    };
  }
}

module.exports = new GitlabProjects();
