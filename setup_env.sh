#!/bin/sh
echo FRONT_END_URL=$FRONT_END_URL >> backend.env
echo GITLAB_APP_CALLBACK=$GITLAB_APP_CALLBACK >> backend.env
echo GITLAB_APP_ID=$GITLAB_APP_ID >> backend.env
echo GITLAB_APP_SECRET=$GITLAB_APP_SECRET >> backend.env
echo POSTGRES_HOST=$POSTGRES_HOST >> backend.env
echo POSTGRES_PORT=$POSTGRES_PORT >> backend.env
echo POSTGRES_USER=$POSTGRES_USER >> backend.env
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >> backend.env
echo POSTGRES_DB=$POSTGRES_DB >> backend.env
echo SESSION_SECRET=$SESSION_SECRET >> backend.env
echo APP_PORT=$APP_PORT >> backend.env
echo CRYPTO_KEY=$CRYPTO_KEY >> backend.env
echo REACT_APP_BACKEND_URL=$REACT_APP_BACKEND_URL >> frontend.env

aws s3 cp backend.env s3://wtg-dev/backend/.env
aws s3 cp frontend.env s3://wtg-dev/frontend/.env